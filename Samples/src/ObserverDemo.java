import java.util.HashSet;
import java.util.Set;


public class ObserverDemo {

	public static void main(String[] args) {
		LameGuy lameGuy = new LameGuy();
		
//		add observers
		Observer doucheBag = new DoucheBag();
		lameGuy.addObserver(doucheBag);
		Observer niceGirl = new NiceGirlThatWillNeverDateLameGuy();
		lameGuy.addObserver(niceGirl);
		
//		setting coolness
		lameGuy.setCoolness("awesome");
		lameGuy.setCoolness("a loser");
	}
	
	public interface Subject {
		public void addObserver(Observer o);
		public void notifyObservers();
	}
	
	public interface Observer {
		public <T> void update(Subject o, T t) throws CoolnessVetoException;
	}
	
	static class LameGuy implements Subject {
		Set<Observer> observers = new HashSet<>();
		String coolness;
		
		public void setCoolness(String coolness) {
			this.coolness = coolness;
			System.out.println(getClass().getName() + ": I'm " + coolness);
			notifyObservers();
		}
		
		public String getCoolness() {
			return coolness;
		}

		@Override
		public void addObserver(Observer o) {
			observers.add(o);
		}

		@Override
		public void notifyObservers() {
			try {
				for (Observer o : observers)
					o.update(this, getCoolness());
			} catch (CoolnessVetoException e) {
				System.out.println(e.getObserver().getClass().getName() + ": " + e.getMessage());
				if (coolness.equals("awesome"))
					coolness = "lame";
				if (coolness.equals("a loser"))
					coolness = "okay";
				System.out.println(this + ": You're right. I'm " + coolness);
			}
		}
		
		@Override
		public String toString() {
			return getClass().getName();
		}
	}
	
	static class DoucheBag implements Observer {

		@Override
		public <T> void update(Subject subject, T coolness) throws CoolnessVetoException {
			if ( (subject instanceof LameGuy) && "awesome".equals(coolness))
				throw new CoolnessVetoException("No you're not, dude; don't lie", this);
		}
		
	}
	
	static class NiceGirlThatWillNeverDateLameGuy implements Observer {

		@Override
		public <T> void update(Subject o, T t) throws CoolnessVetoException {
			if ("a loser".equals(t))
				throw new CoolnessVetoException("Come on, you're not that bad", this);
		}
		
	}
	
	static class CoolnessVetoException extends Exception {
		Observer observer;
		public CoolnessVetoException(String message, Observer observer) {
			super(message);
			this.observer = observer;
		}
		public Observer getObserver() {
			return observer;
		}
	}
	
}
