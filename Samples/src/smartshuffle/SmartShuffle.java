package smartshuffle;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class SmartShuffle {
	
	public static long backtracks = 0;
    
    public static List<Peeps> buildPeeps( List<String> input ) {
    	List<Peeps> result = new ArrayList<>();
    	//create first peep with 1 neighbor
    	result.add( new Peeps( input.get(0), input.get(1) ) );
    	
    	//create all the others with 2 neighbors
    	for( int i = 1; i < input.size() - 1; i++ ) {
    		result.add( new Peeps( input.get(i), input.get(i-1), input.get(i+1) ) );
    	}
    	
    	//except for the last one who just has one neighbor
    	result.add( new Peeps( input.get(input.size() - 1), input.get(input.size() - 2) ) );
    	return result;
    }
    
    //Is the position just before head one of pos's former neighbors?
    private static boolean allowedSwitch( List<Peeps> peeps, int head, int pos) {
        return !peeps.get(pos).neighbors.contains( peeps.get( head - 1 ).name );
    }
    
    private static void swap( List<Peeps> list, int first, int second ) {
        Peeps tmpFirst = list.get( first );
        list.set( first, list.get(second) );
        list.set( second, tmpFirst );
    }
    
    public static boolean shuffle( List<Peeps> peeps ){
        int head = 0;
        int offset = (new Random() ).nextInt( peeps.size() - head - 1) + 1 ;
        int pos = head + offset;

        for( int i = 0; i < peeps.size() - head - 1; i++, pos = head + (offset + i)%(peeps.size() - head) ) {
            swap(peeps, head, pos);
            if( helperShuffle( peeps, head + 1 ) ) {return true;}
            backtracks++;
            swap(peeps, pos, head);
        }
        return false;
    }

    private static boolean helperShuffle( List<Peeps> peeps, int head ) {
    	if ( head == peeps.size() - 1) {
    		return allowedSwitch( peeps, head, head);
    	}
    	
        int offset = (new Random() ).nextInt( peeps.size() - head - 1) + 1 ;
        int pos = head + offset;
        
        for( int i = 0; i < peeps.size() - head - 1; i++, pos = head + (offset + i)%(peeps.size() - head) ) {
            if( allowedSwitch(peeps, head, pos) ) {
                swap(peeps, head, pos);                
                if( helperShuffle( peeps, head + 1 ) ) {return true;}
                backtracks++;
                swap(peeps, pos, head);
            }
        }        
        return false;
    }
    
}