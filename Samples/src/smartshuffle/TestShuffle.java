package smartshuffle;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestShuffle {
	
	static List<String> names;
	
	public TestShuffle() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		
		try {
			setNamesFromFile( args[0] );
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		List<Peeps> allThePeople = SmartShuffle.buildPeeps(names);
		
//		for (Peeps person : allThePeople ) {
//			System.out.println( person.name + " " + person.neighbors);
//		}
		
		long t1 = System.nanoTime();
		
		SmartShuffle.shuffle( allThePeople);
		double t2 = (double)(System.nanoTime() - t1) * 1e-6;
		for (Peeps person : allThePeople ) {
			System.out.println( person.name);
		}
		
		System.out.println();
		System.out.format("%.2f ms%n", t2);
		System.out.println("backtracks: " + SmartShuffle.backtracks);
	}
	
	static void setNamesFromFile( String filename ) throws IOException {
		BufferedReader r = null;
		String s;
		names = new ArrayList<>();
		
		try {
			r = new BufferedReader(new FileReader(filename));
			while ( (s = r.readLine() ) != null )
				names.add(s);
		} finally {
			if ( r != null )
				r.close();
		}
	}
	
}
