package smartshuffle;

import java.util.ArrayList;
import java.util.List;

public class Peeps {
    public String name;
    public List<String> neighbors;
    
    public Peeps(String name, String... people) {
        this.name = name;
        neighbors = new ArrayList<String>();
        for ( String individual : people) {
        	neighbors.add( individual ); 
        }
    }
}