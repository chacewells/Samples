import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class AaronsHashMap {
	private final List<KeyValuePair>[] table;
	int tableSize = 1000;
	
	public AaronsHashMap() {
		table = new List[tableSize];
		for (int i = 0; i < table.length; i++) {
			table[i] = new LinkedList<KeyValuePair>();
		}
	}
	
	public void put(String key, Object value) {
		put(new KeyValuePair(key, value));
	}
	
	private void put(KeyValuePair kv) {
		int hashCode = kv.hashCode();
		if (table[hashCode].contains(kv)) {
			return;
		}
		table[hashCode].add(kv);
	}
	
	public Object get(String key) {
		int hashCode = new KeyValuePair(key, null).hashCode();
		List<KeyValuePair> memLoc = table[hashCode];
		
		for (Iterator<KeyValuePair> itr = memLoc.listIterator(); itr.hasNext();) {
			KeyValuePair pair = itr.next();
			if (key.equals(pair.key)) {
				return pair.value;
			}
		}
		return null;
	}
	
	private class KeyValuePair {
		String key;
		Object value;
		public KeyValuePair(String key, Object value) {
			this.key = key;
			this.value = value;
		}
		public int hashCode() {
			return key.hashCode() % tableSize;
		}
	}
}
