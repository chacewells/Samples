package com.wellsfromwells.samples.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleLoggerServer {
	
	BufferedWriter bWriter;
	PrintWriter writer;
	InputStreamReader input;
	BufferedReader reader;
	
	public void go() {
		try (ServerSocket srvSock = new ServerSocket(5000)) {
			
			while (true) {
				Socket sock = srvSock.accept();
				input = new InputStreamReader(sock.getInputStream());
				reader = new BufferedReader(input);
				bWriter = new BufferedWriter((new FileWriter("C:\\Users\\chq-aaronwe\\log.txt", true)));
				writer = new PrintWriter(bWriter);
				String line = reader.readLine();
				writer.println(line);
				writer.flush();
				
				reader.close();
				writer.close();
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SimpleLoggerServer srv = new SimpleLoggerServer();
		srv.go();
	}

}
