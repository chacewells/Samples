package com.wellsfromwells.samples.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SimpleLoggerClient {
	
	
	public void go() {
		try {
			Socket s = new Socket("127.0.0.1", 5000);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			PrintWriter writer = new PrintWriter(s.getOutputStream());
			
			System.out.println("what's your message?");
			String line = reader.readLine();
			
			writer.println(line);
			writer.flush();
			
			reader.close();
			writer.close();
			
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SimpleLoggerClient client = new SimpleLoggerClient();
		client.go();
	}

}
