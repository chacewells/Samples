package com.wellsfromwales.socket;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class SocketTest {

	public static void main(String[] args) throws IOException
	{
		try
		{
			Socket s = new Socket();
			s.connect(new InetSocketAddress("time-A.timefreq.bldrdoc.gov", 13), 15000);
			InputStream is = s.getInputStream();
			Scanner in = new Scanner(is);
			
			while (in.hasNextLine())
			{
				String line = in.nextLine();
				System.out.println(line);
			}
			
			in.close();
			s.close();
		} catch ( InterruptedIOException e ) {
			e.printStackTrace();
		}
	}

}
