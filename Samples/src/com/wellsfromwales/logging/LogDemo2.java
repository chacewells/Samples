package com.wellsfromwales.logging;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogDemo2 {

	private static final Logger logger = Logger.getLogger(LogDemo2.class
			.getName());
	static {
		try {
			logger.addHandler(new FileHandler("log.txt"));
			logger.setLevel(Level.ALL);
		} catch (SecurityException | IOException e) {
		}
	}

	public static void main(String[] args) {
		System.out.println("begin");
		logger.severe("Oh no!");
		new LogDemo2("arg0").method1();
		System.out.println("end");
		System.out.println(Object.class.getCanonicalName());
		System.out.println(Object.class.getName());

		Method[] objMethods = Object.class.getDeclaredMethods();
		ListIterator<Method> methItr = Arrays.asList(objMethods).listIterator();
		for (Method method; methItr.hasNext();) {
			method = methItr.next();
			System.out.print(method.getName() + "(");
			ListIterator<Parameter> paramItr = Arrays.asList(method.getParameters()).listIterator();
			for (Parameter param; paramItr.hasNext();) {
				param = paramItr.next();
				System.out.print(param.getType().getName());
				System.out.print(param.getName());
				System.out.print((paramItr.hasNext() ? ", " : ""));
			}
			System.out.println(")");
		}
	}

	public LogDemo2(String param) {
		logger.entering(getClass().getName(), "LogDemo2()", param);
		logger.exiting(getClass().getName(), "LogDemo2()", param);
	}

	private void method1() {
		logger.entering(getClass().getName(), "method1()");
		method2();
		method3();
		logger.exiting(getClass().getName(), "method1()");
	}

	private void method2() {
		logger.entering(getClass().getName(), "method2()");
		logger.exiting(getClass().getName(), "method2()");
	}

	private void method3() {
		logger.entering(getClass().getName(), "method3()");
		logger.exiting(getClass().getName(), "method3()");
	}

}
