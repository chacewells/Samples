package com.wellsfromwales.export;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Stack;
import java.util.StringTokenizer;

public class CarriersOnASL {

	public static void main(String[] args) throws IOException {
		BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter the carriers that sail to the destination:");
		System.out.flush();
		String inputLine = keyboard.readLine();
		Collection<String> sailing = new HashSet<>(Arrays.asList(inputLine.split(" ")));
		
		System.out.println("enter the carriers on the ASL:");
		System.out.flush();
		inputLine = keyboard.readLine();
		Collection<String> asl = Arrays.asList(inputLine.split(" "));
		sailing.retainAll(asl);
		
		System.out.println("here are the carriers on both:");
		for ( String carrier : sailing)
			System.out.println(carrier);
	}
	
	public static Collection<String> parseArgs(String argString) {
		StringTokenizer st = new StringTokenizer(argString, "\"", true);
		Collection<String> result = new HashSet<>();
		Stack<String> quotes = new Stack<>();
		StringBuilder sb = new StringBuilder();
		
		while (st.hasMoreElements()) {
			String token = st.nextToken();
			if (token.equals("\"")) {
				if (quotes.empty())
					quotes.push(token);
				else {
					quotes.pop();
					sb.append(token);
					result.add(sb.toString());
					sb = new StringBuilder();
				}
			} else {
				sb.append(token);
			}
		}
		
		return result;
	}

}
