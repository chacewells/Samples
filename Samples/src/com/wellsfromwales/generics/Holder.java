package com.wellsfromwales.generics;

import java.util.ArrayList;
import java.util.List;

public class Holder<T> {
	private T value;
	public T get() {
		return value;
	}
	public void set(T t) {
		value = t;
	}
	
	public <U> List<U> getRandomList() {
		return new ArrayList<U>();
	}
}
