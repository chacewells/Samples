package com.wellsfromwales.generics;

import java.util.Collection;

public class MultipleBoundsDemo {
	
	public static abstract class Mammal {
		private String species;
		public Mammal(String species) {
			this.species = species;
		}
		public abstract void makeNoise();
		public String getSpecies() {return species;}
	}
	
	public interface Pet {
		public void goHome();
	}
	
	public static class Tiger extends Mammal {
		public Tiger() {
			super("tiger");
		}

		@Override
		public void makeNoise() {
			System.out.println("ROAR!");
		}
	}
	
	public static class HouseCat extends Mammal implements Pet {

		public HouseCat() {
			super("house cat");
		}

		@Override
		public void goHome() {
			System.out.println(getSpecies() + ": taking the back alley and catching a mouse");
		}

		@Override
		public void makeNoise() {
			System.out.println("meow!");
		}
	}
	
	public <T extends  Pet> void goHomeAll(Collection<T> pets) {
		for (T pet : pets)
			pet.goHome();
	}
	
	public <T extends Mammal> void makeNoiseAll(Collection<T> mammals) {
		for (T mammal : mammals){
			mammal.makeNoise();
		}
	}
	
	public <T extends Mammal & Pet> void makeNoiseGoHomeAll(Collection<T> mamPets) {
		for (T mamPet : mamPets){
			mamPet.makeNoise();
			mamPet.goHome();
		}
	}

}
