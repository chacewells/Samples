package com.wellsfromwales.generics;

import java.util.Arrays;
import java.util.Random;

public class GenericMethods {

	public static void main(String[] args) {
		String[] strings = {
				"Golf", "Bobble", "Xoo", "Xenon", "Zoo", "Turkey", "Murkey",
				"Jerky", "Work everyday"
		};
		String[] copy = Arrays.copyOf(strings, strings.length);
		Arrays.sort(copy, (l,r) -> l.compareTo(r));
		
		System.out.println(Arrays.asList(copy));
		
		for (String s : copy)
			System.out.println(s + ": " + binarySearch(copy, s));
		
		Integer[] nums = new Integer[2000];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = (new Random()).nextInt(nums.length);
		}
		Arrays.sort(nums);
		
		for (Integer i = 0; i < 2000; i++) {
			int found = binarySearch(nums, i);
			if (found > -1)
				System.out.println("found " + i + " at index " + found);
		}
	}
	
	public static <T extends Comparable<T>> int binarySearch(T[] tArray, T value) {
		int result = -1;
		int guess = tArray.length / 2;
		int min = 0, max = tArray.length - 1;
		
		for ( boolean done = false; 
				min <= max && !done; 
				guess = min + ((max - min) / 2) ) {
			
			if ( value.compareTo(tArray[guess]) == 0) {
				result = guess;
				done = true;
			} else if ( value.compareTo(tArray[guess]) < 0 )
				max = guess - 1;
			else
				min = guess + 1;
			
		}
		
		return result;
	}

}
