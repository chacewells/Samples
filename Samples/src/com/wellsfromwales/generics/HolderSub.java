package com.wellsfromwales.generics;

public class HolderSub<T> extends Holder<T> {
	public HolderSub(T data) {
		super();
		set(data);
	}
	
	@Override
	public String toString() {
		return "Holder<" + get().getClass().getName() + ">: " + get();
	}
	
	public static void main(String[] args) {
		HolderSub<String> holderSub = new HolderSub<>("hello world!");
		System.out.println(holderSub);
		HolderSub<Double> dubbSub = new HolderSub<Double>(15.);
		System.out.println(dubbSub);
	}
}
