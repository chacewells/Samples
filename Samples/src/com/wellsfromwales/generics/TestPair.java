package com.wellsfromwales.generics;

public class TestPair<T> {
	private T actualResult, expectedResult;
	
	public TestPair(T expectedResult, T actualResult) {
		this.actualResult = actualResult;
		this.expectedResult = expectedResult;
	}
	
	public TestPair(T expectedResult) {
		this.expectedResult = expectedResult;
	}
	
	public boolean test() {
		return actualResult.equals( expectedResult);
	}
	
	public T getActualResult() {
		return actualResult;
	}
	
	public void setActualResult(T actualResult) {
		this.actualResult = actualResult;
	}
	
	public T getExpectedResult() {
		return expectedResult;
	}
	
	public void setExpectedResult(T expectedResult) {
		this.expectedResult = expectedResult;
	}
}
