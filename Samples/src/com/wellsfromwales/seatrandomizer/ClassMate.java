package com.wellsfromwales.seatrandomizer;

import java.util.ArrayList;

public class ClassMate {
	private String name;
	private ArrayList<ClassMate> neighbors;
	private int seatNumber;
	
	public ClassMate(String name) {
		this.name = name;
	}
	
	public ClassMate(String name, ClassMate...neighbors) {
		this(name);
		setNeighbors(neighbors);
	}
	
	public ClassMate(String name, ArrayList<ClassMate> neighbors) {
		this(name, (ClassMate[]) neighbors.toArray());
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNeighbors(ClassMate...neighbors) {
		for (ClassMate neighbor : neighbors)
			this.neighbors.add(neighbor);
	}
	
	public void setNeighbors(ArrayList<ClassMate> neighbors) {
		this.neighbors = neighbors;
	}
	
	public ArrayList<ClassMate> getNeighbors() {
		return neighbors;
	}

	public int getSeatNumber() {
		return seatNumber;
	}

	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (this == o) return true;
		if ( !(o instanceof ClassMate) ) return false;
		ClassMate other = (ClassMate) o;
		return getName().equalsIgnoreCase(other.getName());
	}
	
}
