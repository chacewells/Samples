package com.wellsfromwales.seatrandomizer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DoShuffle {

	static List<String> names;
	
	public static void main(String[] args) {
		try {
			setNamesFromFile(args[0]);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		long t1 = System.nanoTime();
		Collections.shuffle(names);
		double t2 = (double)(System.nanoTime() - t1) * 1e-6;
		
		System.out.format("%.2f ms", t2);
		
	}
	
	static void setNamesFromFile( String filename ) throws IOException {
		BufferedReader r = null;
		String s;
		names = new ArrayList<>();
		
		try {
			r = new BufferedReader(new FileReader(filename));
			while ( (s = r.readLine() ) != null )
				names.add(s);
		} finally {
			if ( r != null )
				r.close();
		}
	}

}
