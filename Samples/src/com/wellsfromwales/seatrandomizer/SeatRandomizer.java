package com.wellsfromwales.seatrandomizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class SeatRandomizer {

	private static ArrayList<ClassMate> classMembers = new ArrayList<>();
	
	private static final boolean DEBUG = true;
	
	public static void main(String[] args) {
		
		String 	output1, output2;
		
		try {
			output1 = args[0];
		} catch (ArrayIndexOutOfBoundsException e) {
			output1 = "before.txt";
		}
		
		try {
			output2 = args[1];
		} catch (ArrayIndexOutOfBoundsException e) {
			output2 = "after.txt";
		}
		
		try {
			String input = args[2];
			populateNames( input );
		} catch (ArrayIndexOutOfBoundsException e) {
			populateNames();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		assign();
		display();
		
		writeToFile( output1 );
		
		long t1 = System.nanoTime();
		randomize();
		long t2 = System.nanoTime();
		System.out.println();
		
		assign();
		display();
		if (DEBUG) System.out.format("%.2f ms%n", (double)(t2 - t1)*1e-6);
		
		writeToFile( output2 );
		
	}
	
	static void populateNames() {
		InputStreamReader stdin = null;
		try {
			stdin = new InputStreamReader(System.in);
			populateNames( stdin );
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stdin != null)
					stdin.close();
			} catch (IOException e) {
			}
		}
	}
	
	static void populateNames( String file ) throws FileNotFoundException {
		FileReader fReader = null;
		try {
			fReader = new FileReader(file);
			populateNames( fReader );
		} finally {
				try {
					if (fReader != null)
						fReader.close();
				} catch (IOException e) {
				}
		}
	}
	
	static void populateNames( Reader baseReader ) throws FileNotFoundException {
		BufferedReader reader = null;
		String rString;
		
		try {
			reader = new BufferedReader(baseReader);
			
			while ( ( rString = reader.readLine() ) != null ) {
				if (DEBUG) System.out.println("populateNames(String)");
				classMembers.add(new ClassMate(rString));
			}
		} catch (IOException e) {
			if (e instanceof FileNotFoundException)
				throw new FileNotFoundException();
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
			}
		}
	}
	
	private static void randomize() {
		if ( !neighborsAssigned() ) {
			Collections.shuffle(classMembers);
			return;
		}
		while ( hasSameNeighbors() || hasSameSeats() ) {
			Collections.shuffle(classMembers);
		}
	}
	
	private static boolean neighborsAssigned() {
		boolean result = false;
		outer:
		for (ClassMate member : classMembers) {
			if (member.getNeighbors() == null)
				continue;
			for (ClassMate neighbor : member.getNeighbors()) {
				if (neighbor != null) {
					result = true;
					break outer;
				}
			}
		}
		
		return result;
	}
	
	private static boolean hasSameNeighbors() {
		boolean result = false;
		Iterator<ClassMate> itr = classMembers.iterator();
		ClassMate current;
		int idx;
		ClassMate last, next;
		
		while (itr.hasNext()) {
			idx = classMembers.indexOf(itr.next());
			current = classMembers.get(idx);
			
			if (current.getNeighbors() == null)
				break;
			
			if (idx > 0) {
				last = classMembers.get(idx - 1);
				if ( current.getNeighbors().contains(last) ) {
					result = true;
					break;
				}
			}
			if (idx < (classMembers.size() - 1) ) {
				next = classMembers.get(idx + 1);
				if ( current.getNeighbors().contains(next) ) {
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
	
	private static void assignNeighbors() {
		Iterator<ClassMate> itr = classMembers.iterator();
		int idx;
		ArrayList<ClassMate> neighbors;
		ClassMate current;
		
		while ( itr.hasNext() ) {
			neighbors = new ArrayList<>();
			idx = classMembers.indexOf( itr.next() );
			current = classMembers.get(idx);
			
			if (idx > 0)
				neighbors.add(classMembers.get(idx - 1));
			if ( idx < (classMembers.size() - 1) )
				neighbors.add(classMembers.get(idx + 1));
			
			current.setNeighbors(neighbors);
		}
	}
	
	private static void display() {
		ArrayList<ClassMate> neighbors;
		int inx = 0;
		for (ClassMate member : classMembers) {
			neighbors = member.getNeighbors();
			System.out.println(++inx + ") " + member.getName() + ":");
			System.out.print("\tneighbors: ");
			for (ClassMate neighbor : neighbors)
				System.out.print(neighbor.getName() + ", ");
			
			System.out.println();
		}
	}
	
	private static void writeToFile(String filename) {
		File file = new File(filename);
		PrintWriter fw = null;
		
		try {
			fw = new PrintWriter(file);
			for (ClassMate m : classMembers)
				fw.println(/*classMembers.indexOf(m) + ": " + */m.getName());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (fw != null)
				fw.close();
		}
	}
	
	private static void assignSeats() {
		for ( int inx = 0; inx < classMembers.size(); inx++)
			classMembers.get(inx).setSeatNumber(inx);
	}
	
	private static boolean hasSameSeats() {
		boolean result = false;
		for ( int inx = 0; inx < classMembers.size(); inx++) {
			if (inx == classMembers.get(inx).getSeatNumber()) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	private static void assign() {
		assignSeats();
		assignNeighbors();
	}
}
