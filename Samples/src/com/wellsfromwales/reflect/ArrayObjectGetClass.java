package com.wellsfromwales.reflect;

public class ArrayObjectGetClass {

	public static void main(String[] args) {
		System.out.println( new int[10].getClass() );
		System.out.println( new int[10][10].getClass() );
		System.out.println( new int[10][10][10].getClass() );
		System.out.println( new String[10].getClass() );
	}

}
