package com.wellsfromwales.reflect;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeclaredClassWithAnonymous {
	
	public static class OneDeclaredStaticInner {}
	
	private class TwoDeclaredInner {}
	
	private ActionListener al = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("This is me doing something cool.");
		}
	};

	public static void main(String[] args) {
		Class<?>[] classes = DeclaredClassWithAnonymous.class.getDeclaredClasses();
		for (Class<?> c : classes)
			System.out.println(c.toString());
	}

}

class InternalClass {}