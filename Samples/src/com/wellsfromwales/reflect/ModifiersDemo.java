package com.wellsfromwales.reflect;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class ModifiersDemo {
	public class CModDemo1 {}
	public abstract class CModDemo2 {}
	final class CModDemo3 {}
	public class CModDemo4 extends ArrayList<Integer> {}
	public class CModDemo5 implements ActionListener {@Override public void actionPerformed(ActionEvent e) {}}
	public interface CModDemo6 {}

	public static void main(String[] args) {
		Class<?>[] objs = ModifiersDemo.class.getDeclaredClasses();
		
		for ( Class<?> o : objs )
			System.out.println( Modifier.toString(o.getModifiers()) );
	}
	
	public static String modToString(int mods) {
		StringBuilder sb = new StringBuilder();
		if ( (mods & Modifier.ABSTRACT) != 0) sb.append("ABSTRACT ");
		if ( (mods & Modifier.FINAL) != 0) sb.append("FINAL ");
		if ( (mods & Modifier.PUBLIC) != 0) sb.append("PUBLIC ");
		if ( (mods & Modifier.PRIVATE) != 0) sb.append("PRIVATE");
		if ( (mods & Modifier.INTERFACE) != 0) sb.append("INTERFACE ");
		if ( (mods & Modifier.STATIC) != 0) sb.append("STATIC");
		
		return sb.toString();
	}

}
