package com.wellsfromwales.samples;

public class Parrot extends Bird {
	
	public static void main(String...args) {
		Bird parrot = new Parrot();
		parrot.sound(3);
	}
	
	public Parrot() {
		super();
	}
	
	public void sound() {
		System.out.println("awk");
	}
	
}
