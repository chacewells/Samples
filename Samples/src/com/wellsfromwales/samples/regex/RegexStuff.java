package com.wellsfromwales.samples.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RegexStuff {
	
	private RegexStuff() {}

	public static void main(String[] args) {
		matchesExample();
		splitExample();
		replaceAllExample();
		patternMatcherClassesExample();
	}
	
	static void matchesExample() {
		String str = "1.21";
		String regex = "\\d+\\.\\d+";
		boolean doesItMatch = str.matches(regex);
		System.out.format("True or false: \"%s\" matches '%s': %b%n", str, regex, doesItMatch);
		
		str = "I need 1.21 gigawatts!";
		doesItMatch = str.matches(regex);
		System.out.format("True or false: \"%s\" matches '%s': %b%n", str, regex, doesItMatch);
		
		regex = ".*" + regex + ".*";
		doesItMatch = str.matches(regex);
		System.out.format("True or false: \"%s\" matches '%s': %b%n", str, regex, doesItMatch);
		
		str = "Emily45";
		regex = "[Ee]mily\\d*";
		doesItMatch = str.matches(regex);
		System.out.format("True or false: \"%s\" matches '%s': %b%n", str, regex, doesItMatch);
		
		str = "emily";
		doesItMatch = str.matches(regex);
		System.out.format("True or false: \"%s\" matches '%s': %b%n", str, regex, doesItMatch);
		
		str += "_";
		doesItMatch = str.matches(regex);
		System.out.format("True or false: \"%s\" matches '%s': %b%n", str, regex, doesItMatch);
		System.out.println();
	}
	
	static void splitExample() {
		String url = "http://www.ireallylikehotdogs.uk:80/path/to/hotdogs?eat=yes#yum";
		System.out.println("URL: " + url);
		
//		parse the protocol
		String regex = "://";
		String[] strArray = url.split(regex, 2);
		String protocol = strArray[0];
		System.out.println("Protocol: " + protocol);
		url = strArray[1];
		
//		parse the domain
		regex = "[:/]";
		strArray = url.split(regex, 2);
		String domain = strArray[0];
		System.out.println("Domain: " + domain);
		url = strArray[1];
		
//		parse the port
		regex = "(/|$)"; // figure this one out
		strArray = url.split(regex, 2);
		String sPort;
		int port = -1;
		if ( strArray.length > 1 ) {
			sPort = strArray[0];
			if (sPort.matches("\\d+"))
				port = Integer.parseInt(sPort);
			else
				System.out.println( "Invalid port detected: " + sPort );
		}
		System.out.println("Port: " + ( (port > -1) ? port : "none" ) );
		
//		etc...
		System.out.println();
	}
	
	static void replaceAllExample() {
		String str = "what the hell do you want you little Hellion?";
		System.out.println("offensive: " + str);
		
		String regex = "[Hh]ell"; // find the offensive part
		str = str.replaceAll(regex, "smell"); // replace it with something more mild
		System.out.println("inoffensive: " + str);
		System.out.println();
	}
	
	static void patternMatcherClassesExample() {
		System.out.println();
		String regex = "[Ll]apdogs?";
		
//		the pattern object containing the regular expression
		Pattern pattern = Pattern.compile(regex);
		
//		The string to search on
		String searchOn = "\"I found these new lapdogs with happy lapdog tags. you can find them at the "
				+ "lapdog store in Lapdog Central\"";
		System.out.println("Searching this string:");
		System.out.println(searchOn + "\n");
		
//		this is an object that iterates over the matches in the searched-on string
		Matcher matcher = pattern.matcher(searchOn);
		
//		let's see how many times the pattern shows up in the searchOn string
		int count = 0;
		while (matcher.find())
			count++;
		System.out.println("number of matches for '" + regex + "': " + count);
		
//		this time we'll reset the matcher and print every match that shows up
		matcher = pattern.matcher(searchOn);
		System.out.print("Here are the matches: ");
		while (matcher.find())
			System.out.print( matcher.group() + " " );
		System.out.println();
	}

}
