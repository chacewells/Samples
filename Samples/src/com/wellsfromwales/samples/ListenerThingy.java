package com.wellsfromwales.samples;

import java.awt.event.ActionListener;

import javax.swing.JWindow;

public class ListenerThingy extends JWindow implements Runnable {
	
	ActionListener exitListener = e -> System.exit(0);
	ActionListener showListener = e -> System.out.println("show logic");
	ActionListener hideListener = e -> System.out.println("hide logic");
	
	@Override
	public void run() {
		
	}
}
