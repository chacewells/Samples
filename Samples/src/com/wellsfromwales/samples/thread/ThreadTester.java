package com.wellsfromwales.samples.thread;


public class ThreadTester {

	public static void main(String[] args) throws InterruptedException {
		Thread t = new Thread(myRunnable);
		t.start();
		Thread.sleep(1000);
		System.out.println("back in main");
	}
	
	private static Runnable myRunnable = () -> {
		Runnable otherRunnable = () -> System.out.println("this is the other one.");
		Thread t = new Thread(otherRunnable);
		t.start();
		try {
			Thread.sleep(800);
		} catch ( InterruptedException e ) {
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("top o' the stack");
	};

}
