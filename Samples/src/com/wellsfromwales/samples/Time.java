package com.wellsfromwales.samples;

public class Time {
	private int hour, min, sec;
	
	public Time( int hour, int min, int sec) {
		this.setHour(hour).setMin(min).setSec(sec);
	}
	
	public Time( Time time ) {
		this( time.getHour(), time.getMin(), time.getSec() );
	}

	public int getHour() {
		return hour;
	}

	public Time setHour(int hour) {
		this.hour = hour % 24;
		return this;
	}

	public int getMin() {
		return min;
	}

	public Time setMin(int min) {
		if (min > 60)
			hour += ( min / 60 );
		this.min = min % 60;
		return this;
	}

	public int getSec() {
		return sec;
	}

	public Time setSec(int sec) {
		if (sec > 60)
			min += sec / 60;
		this.sec = sec % 60;
		return this;
	}
	
	public Time add( Time toAdd ) {
		int		hour = getHour() + toAdd.getHour(),
				min = getMin() + toAdd.getMin(),
				sec = getSec() + toAdd.getSec();
		
		return new Time( hour, min, sec );
	}
	
	public String toString() {
		return (new StringBuilder())
				.append(getHour())
				.append(':')
				.append(getMin())
				.append(':')
				.append(getSec())
				.toString();
	}

}
