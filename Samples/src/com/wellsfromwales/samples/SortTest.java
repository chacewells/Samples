package com.wellsfromwales.samples;

import java.util.Arrays;
import java.util.Comparator;

public class SortTest {

	public static void main(String[] args) {
		String[] myStrings = {
				"baking",
				"toasting",
				"pumping",
				"churning",
				"learning",
				"burning",
				"earning",
				"earrings",
				"ironing",
				"darning",
				"morning",
				"corning"
		};
		
		for ( String s : myStrings )
			System.out.println(s);
		
		System.out.println();
		
		Comparator<String> comp = new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return -o1.compareTo(o2);
			}
		};
		
		Arrays.sort(myStrings, comp);
		
		for (String s : myStrings )
			System.out.println(s);
	}

}
