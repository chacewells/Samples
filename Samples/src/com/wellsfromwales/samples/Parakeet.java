package com.wellsfromwales.samples;

public class Parakeet extends Parrot {
	
	public Parakeet() {
		super();
	}

	@Override
	public void sound() {
		System.out.println("tweet");
	}

}
