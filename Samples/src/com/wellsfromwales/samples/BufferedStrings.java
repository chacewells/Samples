package com.wellsfromwales.samples;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class BufferedStrings {

	public static void main(String[] args) throws FileNotFoundException {
		
		BufferedReader reader = 
				new BufferedReader(
				new FileReader(
						"C:\\Users\\chq-aaronwe\\Documents\\tmp\\hello.txt"));
		
		String str;
		
		try {
			while ((str = reader.readLine()) != null) {
				System.out.println(str);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
