package com.wellsfromwales.samples;

import java.util.Iterator;


public class SimpleQueue<T> implements Iterable<T> {
	QNode head;
	QNode tail;
	
	public static void main(String...args) {
		SimpleQueue<String> q = new SimpleQueue<>();
		q.push("hello");
		q.push("node");
		q.push("world");
		q.push("donkey kong");
		
		Iterator<String> itr = q.iterator();
		while (itr.hasNext())
			System.out.println(itr.next());
	}
	
	public SimpleQueue() {
		head = tail = null;
	}
	
	public void push( T data ) {
		push( new QNode(data) );
	}
	
	private void push( QNode node ) {
		if (tail == null) {
			tail = head = node;
		} else {
			node.setPrevious(tail);
			tail.setNext(node);
			tail = node;
		}
	}
	
	public T pop() {
		T out = null;
		if ( head != null ) {
			out = head.getData();
			head = head.getNext();
		}
		
		return out;
	}
	
	private class QNode {
		QNode next;
		QNode previous;
		T data;
		
		public QNode( T data ) {
			setData(data);;
		}

		public QNode getNext() {
			return next;
		}

		public void setNext(QNode next) {
			this.next = next;
		}

		public QNode getPrevious() {
			return previous;
		}

		public void setPrevious(QNode previous) {
			this.previous = previous;
		}

		public T getData() {
			return data;
		}

		public void setData(T data) {
			this.data = data;
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				return head != null;
			}

			@Override
			public T next() {
				return pop();
			}
			
		};
	}
	
}
