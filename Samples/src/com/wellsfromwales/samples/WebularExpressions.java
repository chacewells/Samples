package com.wellsfromwales.samples;

public class WebularExpressions {

	public static void main(String[] args) {
		String toParse = "My social security number is 226-11-6622";
		String regex = "\\b\\d{3}-\\d{2}";
		String parsed = toParse.replaceAll(regex, "XXX-XX");
		System.out.println(toParse);
		System.out.println(parsed);
	}

}
