package com.wellsfromwales.samples;

public class HexFormat {

	public static void main(String[] args) {
		int hex = 0xF;
		String hexString = String.format("%02x", hex).toString().toUpperCase();
		
		System.out.format("using string format: %c%s%n", '%', hexString);
		System.out.println("using toHexString: " + Integer.toHexString(hex).toUpperCase());
		
	}
	
}
