package com.wellsfromwales.samples;

import java.util.Stack;

public class Reversi {

	public static void main(String[] args) {
		System.out.println(printo(896013));
		System.out.println( printoUsingJavaStack(896013) );
		System.out.println( printoUsingDirtyStack(896013) );
	}
	
	static String printo( int num ) {
		if ( num <= 0)
			return "";
		int temp = num;
		int digit = temp % 10;
		return printo(temp /= 10) + digit;
	}
	
	static String printoUsingJavaStack( int num ) {
		Stack<Integer> intStack = new Stack<>();
		StringBuilder sb = new StringBuilder();
		int temp = num;
		while ( temp > 0 ) {
			intStack.push( temp % 10 );
			temp /= 10;
		}
		
		while ( ! intStack.empty() )
			sb.append( intStack.pop() );
		
		return sb.toString();
	}
	
	static String printoUsingDirtyStack( int num ) {
		DirtyStack intStack = new DirtyStack();
		StringBuilder sb = new StringBuilder();
		int temp = num;
		while ( temp > 0 ) {
			intStack.push( temp % 10 );
			temp /= 10;
		}
		
		while ( intStack.head != null )
			sb.append( intStack.pop() );
		
		return sb.toString();
	}

}
