package com.wellsfromwales.samples;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class ParseFromStdIn {
	
	private static BufferedReader reader;

	public static void main(String[] args) throws IOException {
//		String data = getInput();
//		StringTokenizer t;
//		
//		while ( ! data.isEmpty() ) {
//			t = new StringTokenizer(data, ", ");
//			while ( t.hasMoreTokens() )
//				System.out.println( t.nextToken() );
//			data = getInput();
//		}
//		
//		String dickJaneSally = "dick,,,jane,,,sally,,,";
//		t = new StringTokenizer(dickJaneSally, ",");
//		
//		while ( t.hasMoreTokens() )
//			System.out.print( t.nextToken() );
		String data = getInput();
		StringTokenizer t = new StringTokenizer( data, ", " );
		
		double sum = 0.;
		int count = 0;
		while ( t.hasMoreTokens() ) {
			sum += Double.parseDouble( t.nextToken() );
			count++;
		}
		
		System.out.println( "average: " + sum / (double)count );
		
	}
	
	public static String getInput() throws IOException {
		if ( reader == null ) {
			InputStreamReader stdin = new InputStreamReader( System.in );
			reader = new BufferedReader( stdin );
		}
		
		System.out.println( "Enter numbers separated by spaces or commas" );
		String input = reader.readLine();
		return input;
	}

}
