package com.wellsfromwales.samples;

public class ImportantPerson extends Person {
	
	private String birthday;
	
	public ImportantPerson() {
		birthday = "none";
	}
	
	public ImportantPerson( String firstName, String lastName ) {
		super(firstName, lastName);
		birthday = "none";
	}
	
	public String getBirthday() {
		return birthday;
	}

	public ImportantPerson setBirthday(String birthday) {
		this.birthday = birthday;
		return this;
	}

	public static void main(String[] args) {
		System.out.println((new ImportantPerson("Alex", "Bogo")).setBirthday("Dec 21, 2004"));
	}
	
	@Override
	public String toString() {
		return (new StringBuilder(super.toString()))
				.append(", ")
				.append(getBirthday())
				.toString();
	}
	
}
