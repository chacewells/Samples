package com.wellsfromwales.samples;

import java.awt.event.ActionEvent;

import javax.swing.JButton;

public class Types {

	public static void main(String[] args) {
			long temp = 5l;
			int inx = 1;
			double dnum = 5.;
			System.out.println( ((Object)((int)dnum/inx)).getClass() );
			System.out.println( ( (Object)(dnum < inx) ).getClass() );
			System.out.println( ((Object)(temp*inx)).getClass() );
			System.out.println(((Object)(temp*inx/dnum)).getClass());
	}
	
	public void actionPerformed( ActionEvent event )
	{
	    Object  source  = event.getSource();
	    if ( source instanceof JButton )
	    {
	        JButton button  = (JButton)source;
	        processEvent( button );
	    }

	    System.out.println( "show logic" );
	}
	
	void processEvent( JButton button ) {}

}
