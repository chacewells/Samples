package com.wellsfromwales.samples;

public class DumbTry {
	public static void main(String[] args) {
		try {
			method();
		} catch (Exception e) {
			System.out.println("main catch");
		}
	}
	
	static void method() throws Exception {
		try {
			throw new Exception();
		} finally {
			System.out.println("finally block");
		}
	}
}
