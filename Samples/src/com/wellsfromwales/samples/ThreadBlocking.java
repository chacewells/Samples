package com.wellsfromwales.samples;

public class ThreadBlocking {
	
	private static Object lock = new Object();
	
	public static void main(String...args) {
		MyRunnable mr = new MyRunnable();
		
		Thread t1 = new Thread(mr, "thread 1");
		Thread t2 = new Thread(mr, "thread 2");
		Thread t3 = new Thread(mr, "thread 3");
		Thread t4 = new Thread(mr, "thread 4");
		
		for ( Thread t : new Thread[]{t1,t2,t3,t4})
			t.start();
		
		lock.notify();
	}
	
	static class MyRunnable implements Runnable {

		@Override
		public void run() {
			synchronized (lock) {
				Thread t = Thread.currentThread();
				try {
					lock.wait();
					System.out.println(t.getName() + " finishing.");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
}
