package com.wellsfromwales.samples;

import java.util.Stack;
import java.util.StringTokenizer;

public class Parentheses {

	public static void main(String[] args) {
		System.out.println( "5+5 = " + evaluate("5+5") );
		System.out.println( "5+5*2+1 = " + evaluate( "5+5*2+1" ) );
		System.out.println( "(1+3)*9 = " + evaluate( "(1+3)*9" ) );
	}
	
	static double evaluate( String str ) {
		double result = 0.;
		StringTokenizer tizer = new StringTokenizer(str, "()+-*/", true);
		Stack<String> operators = new Stack<>(),
				values = new Stack<>();
		String token;
		
		while (tizer.hasMoreTokens()) {
			token = tizer.nextToken();
			if ( isOperator(token) ) {
				if ( operators.empty() ||
						token.equals( "(" ) ||
						operators.peek().equals( "(" ) )
					operators.push(token);
				else if ( token.equals( ")" ) ) {
					String op;
					while ( !(op = operators.pop()).equals( "(" ) ) {
						String val2 = values.pop();
						String val1 = values.pop();
						values.push( evaluate( op, val1, val2 ) );
					}
				} else if ( token.matches("[\\Q+-*/\\E]") ) {
					if ( getPrecedence(token) > getPrecedence( operators.peek() ) ) {
						operators.push(token);
					} else {
						String val2 = values.pop();
						values.push( evaluate(operators.pop(), values.pop(), val2) );
						operators.push(token);
					}
				}
			} else
				values.push(token);
		}
		
		while( !operators.empty() ) {
			String val2 = values.pop();
			String val1 = values.pop();
			String op = operators.pop();
			values.push( evaluate(op, val1, val2) );
		}
		
		result = Double.parseDouble(values.pop());
		return result;
	}
	
	static boolean isOperator(String token) {
		return "+-*/()".contains(token);
	}
	
	static String evaluate(String operator, String operand1, String operand2) {
		double result = 0.;
		double op1 = Double.parseDouble(operand1),
				op2 = Double.parseDouble(operand2);
		
		switch(operator) {
		case "+": result = op1 + op2;break;
		case "-": result = op1 - op2;break;
		case "*": result = op1 * op2;break;
		case "/": result = op1 / op2;break;
		default: throw new InvalidOperatorException("" + operator + " is not a valid operator");
		}
		
		return Double.toString(result);
	}
	
	static int getPrecedence(String operator) {
		switch(operator) {
		case "+":case "-":
			return 1;
		case "*":case "/":
			return 2;
		case "(":
			return 3;
		default:throw new InvalidOperatorException("" + operator + " is not a valid operator");
		}
	}
	
	public static class InvalidOperatorException extends IllegalArgumentException {
		public InvalidOperatorException() {super();}
		public InvalidOperatorException(String message, Throwable cause) {super(message, cause);}
		public InvalidOperatorException(String s) {super(s);}
		public InvalidOperatorException(Throwable cause) {super(cause);}
	}

}
