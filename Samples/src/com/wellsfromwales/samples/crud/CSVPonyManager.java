package com.wellsfromwales.samples.crud;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public final class CSVPonyManager {
	
	PrintWriter writer;
	BufferedReader reader;
	
	public static final String[] NAMES = {
		"Clive",
		"Winston",
		"Gabby",
		"Charlie",
		"Judy",
		"Anne"
	};
	
	public static final int[][] YMD = {
		{2012,7,14},
		{2010,6,1},
		{2011,1,2},
		{2013,4,5},
		{2010,6,1},
		{2014,7,30}
	};
	
	public static final Color[] COLORS = {
		Color.BLUE,
		Color.PINK,
		Color.MAGENTA,
		Color.CYAN,
		Color.RED,
		Color.YELLOW
	};
	
	public static void main(String[] args) {
		CSVPonyManager man = new CSVPonyManager("ponydb.txt");
		
		List<Pony> fetched = man.readPonyList();
		fetched
		.stream()
		.sorted()
		.forEach(System.out::println);
		
	}
	
	private String fileName;

	public CSVPonyManager( String fileName ) {
		this.fileName = fileName;
	}
	
	public void writePony( Pony pony ) {
		String csv = pony.getCSV();
		PrintWriter writer;
		try {
			writer = getWriter();
			writer.println(csv);
			writer.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void overwrite( Collection<Pony> ponies ) {
		PrintWriter overwriter;
		try {
			overwriter = getOverwriter();
			for ( Pony p : ponies )
				overwriter.println(p.getCSV());
			overwriter.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void overwrite( Pony pony ) {
		PrintWriter overwriter;
		try {
			overwriter = getOverwriter();
			overwriter.println( pony.getCSV() );
			overwriter.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void writePonies( Collection<Pony> ponies ) {
		for (Pony p : ponies)
			writePony(p);
	}
	
	public List<Pony> readPonyList() {
		List<Pony> ponyList = new ArrayList<>();
		String csv;
		
		try {
			BufferedReader reader = getReader();
			while ( reader.ready() ) {
				csv = reader.readLine();
				Pony pony = Pony.getInstance(csv);
				ponyList.add(pony);
			}
		} catch (FileNotFoundException e) {
			System.err.println("file not found");
		} catch (IOException e) {
			System.err.println("error reading from Pony database");
		}
		
		return ponyList;
	}
	
	public Set<Pony> readPonySet() {
		Set<Pony> pSet = new TreeSet<>( readPonyList() );
		return pSet;
	}
	
	private PrintWriter getWriter() throws FileNotFoundException {
		if (writer == null) {
			File file = new File(fileName);
			FileOutputStream out = new FileOutputStream(file, true);
			writer = new PrintWriter(out);
		}
		
		return writer;
	}
	
	private PrintWriter getOverwriter() throws FileNotFoundException {
		File file = new File(fileName);
		FileOutputStream out = new FileOutputStream(file, false);
		writer = new PrintWriter(out);
		
		return writer;
	}
	
	private BufferedReader getReader() throws FileNotFoundException {
		File file = new File(fileName);
		FileReader in = new FileReader(file);
		reader = new BufferedReader(in);
		
		return reader;
	}
	
}
