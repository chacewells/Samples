package com.wellsfromwales.samples.crud;

import java.awt.Color;
import java.util.Calendar;

public class Pony implements Comparable<Pony> {
	
	private String name;
	private Color color;
	private Calendar releaseDate;
	
	public Pony() {
		this.name = "";
		this.color = Color.WHITE;
		this.releaseDate = Calendar.getInstance();
	}
	
	public static Pony getInstance( String csv ) {
		Pony pony = null;
		String[] props = csv.split(",");
		String name = props[0];
		int colorInt = Integer.parseInt(props[1]);
		Color color = new Color(colorInt);
		String dateString = props[2];
		String[] dateComponents = dateString.split("-");
		int year = Integer.parseInt(dateComponents[0]);
		int month = Integer.parseInt(dateComponents[1]);
		int dayOfMonth = Integer.parseInt(dateComponents[2]);
		
		pony = (new Pony()).setName(name).setColor(color).setReleaseDate(year, month, dayOfMonth);
		return pony;
	}
	
	public String getName() {
		return name;
	}
	public Pony setName(String name) {
		this.name = name;
		return this;
	}
	public Color getColor() {
		return color;
	}
	public Pony setColor(Color color) {
		this.color = color;
		return this;
	}
	public Calendar getReleaseDate() {
		return this.releaseDate;
	}
	public Pony setReleaseDate( Calendar calendar ) {
		Calendar c = releaseDate;
		c.set(calendar.get(Calendar.YEAR), Calendar.MONTH, Calendar.DAY_OF_MONTH);
		
		return this;
	}
	public Pony setReleaseDate( int year, int month, int dayOfMonth ) {
		Calendar c = releaseDate;
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DATE, dayOfMonth);
		
		return this;
	}
	
	public String getReleaseDateString() {
		StringBuilder sb = new StringBuilder();
		sb.append(releaseDate.get(Calendar.YEAR));
		sb.append("-");
		sb.append(releaseDate.get(Calendar.MONTH));
		sb.append("-");
		sb.append(releaseDate.get(Calendar.DATE));
		
		return sb.toString();
	}
	
	public int getColorRGB() {
		return color.getRGB();
	}
	
	public String getCSV() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(getName());
		sb.append(',');
		sb.append(getColorRGB());
		sb.append(',');
		sb.append(getReleaseDateString());
		
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return "Pony [name=" + name + ", color=" + color + ", releaseDate="
				+ getReleaseDateString() + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( !(obj instanceof Pony) ) return false;
		if ( this == obj ) return true;
		Pony o = (Pony)obj;
		boolean sameName = getName().equals(o.getName());
		boolean sameColor = getColor().equals(o.getColor());
		boolean sameRelease = getReleaseDate().equals(o.getReleaseDate());
		boolean same = sameName && sameColor && sameRelease;
		
		return same;
	}

	@Override
	public int compareTo(Pony o) {
		if ( getName().compareTo(o.getName()) != 0 )
			return getName().compareTo(o.getName());
		if ( getReleaseDate().compareTo( o.getReleaseDate() ) != 0 )
			return getReleaseDate().compareTo( o.getReleaseDate() );
		
		return getColorRGB() - o.getColorRGB();
	}
	
}
