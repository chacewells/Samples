package com.wellsfromwales.samples;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StringFormatting {

	public static void main(String[] args) {
		Date d = new Date(System.currentTimeMillis());
		List<String> ds = new ArrayList<>();
		
		ds.add( String.format("%tc", d) );
		ds.add( String.format("%tr", d) );
		ds.add( String.format("%tA, %<tB %<td", d) );
		for ( String s : ds )
			System.out.println(s);
	}

}
