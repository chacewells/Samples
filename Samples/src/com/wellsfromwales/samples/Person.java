package com.wellsfromwales.samples;

public class Person {
	private String firstName, lastName;
	
	public Person() {
		firstName = lastName = "none";
	}
	
	public Person( String firstName, String lastName ) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public Person setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Person setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}
	
	@Override
	public String toString() {
		return (new StringBuilder())
				.append(getFirstName())
				.append(' ')
				.append(getLastName()).toString();
	}

	public static void main(String[] args) {
		Person gw = (new Person())
				.setFirstName("George")
				.setLastName("Washington");
		
		System.out.println(gw);
	}
	
}
