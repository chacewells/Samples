package com.wellsfromwales.samples;

public class InnerAccessInner {
	
	public static void main(String...args) {
		InnerAccessInner iai = new InnerAccessInner();
		Hopper h = iai.getHopper();
		h.hop();
	}
	
	private class Bunny {
		private int data = 15;
	}
	
	private class Hopper {
		Bunny b = new Bunny();
		
		void hop() {
			System.out.println(b.data);
		}
	}
	
	public Hopper getHopper() {
		return new Hopper();
	}
}
