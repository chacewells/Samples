package com.wellsfromwales.samples.collections;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AsList {
	
	static Map<String, String> myMap = new HashMap<String, String>();
	static {
		myMap.put("hello", "world");
		myMap.put("silly", "map");
		myMap.put("trix", "are");
		myMap.put("for", "kids");
		myMap = Collections.unmodifiableMap(myMap);
	}

	public static void main(String[] args) {
		List<String> list = Arrays.asList(new String[]{
				"string", "two", "three", "four"
		});
		
		String[] stRay = (String[])list.toArray();
		for ( String str : stRay )
			System.out.println(str);
	}

}
