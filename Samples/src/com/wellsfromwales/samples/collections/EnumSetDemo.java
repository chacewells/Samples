package com.wellsfromwales.samples.collections;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Set;

public class EnumSetDemo {

	public static void main(String[] args) {
		EnumSet<NbaTeams> all = EnumSet.allOf(NbaTeams.class);
		EnumSet<NbaTeams> none = EnumSet.noneOf(NbaTeams.class);
		
		Set<NbaTeams> southwest = EnumSet.of(
				NbaTeams.MAVERICKS,
				NbaTeams.ROCKETS,
				NbaTeams.GRIZZLIES,
				NbaTeams.PELICANS,
				NbaTeams.SPURS);
		
		Set<NbaTeams> northwest = EnumSet.of(
				NbaTeams.NUGGETS,
				NbaTeams.TIMBERWOLVES,
				NbaTeams.THUNDER,
				NbaTeams.BLAZERS,
				NbaTeams.JAZZ);
		
		Set<NbaTeams> pacific = EnumSet.of(
				NbaTeams.WARRIORS,
				NbaTeams.CLIPPERS,
				NbaTeams.LAKERS,
				NbaTeams.SUNS,
				NbaTeams.KINGS);
		
		Set<NbaTeams> west = ((EnumSet<NbaTeams>)southwest).clone();
		west.addAll(northwest);
		west.addAll(pacific);
		
		Set<NbaTeams> east = EnumSet.complementOf((EnumSet<NbaTeams>)west);
		
		Set<NbaTeams> top10 = EnumSet.of(NbaTeams.GRIZZLIES, NbaTeams.ROCKETS, 
				NbaTeams.RAPTORS, NbaTeams.WARRIORS, NbaTeams.WIZARDS,
				NbaTeams.BULLS, NbaTeams.BLAZERS, NbaTeams.MAVERICKS,
				NbaTeams.HORNETS, NbaTeams.SPURS);
		
		Set<NbaTeams> topInWest = ((EnumSet<NbaTeams>)west).clone();
		topInWest.retainAll(top10);
		
		Set<NbaTeams> topInEast = ((EnumSet<NbaTeams>)east).clone();
		topInEast.retainAll(top10);
		
		System.out.println("Top west teams:");
		for ( NbaTeams team : topInWest )
			System.out.println(team.toString());
		System.out.println();
		
		System.out.println("Top east teams:");
		for ( NbaTeams team : topInEast )
			System.out.println(team.toString());
		System.out.println();
		
		Set<NbaTeams> lowerTwoThirds = EnumSet.complementOf((EnumSet<NbaTeams>)top10);
		
		System.out.println("Lower two thirds of NBA:");
		for ( NbaTeams team : lowerTwoThirds )
			System.out.println(team.toString());
		
		NbaTeams[] lowerArray = lowerTwoThirds.toArray(new NbaTeams[lowerTwoThirds.size()]);
		System.out.println();
		Arrays.sort(lowerArray, (a,b) -> -( a.toString().compareTo( b.toString() ) ));
		System.out.println("Lower two thirds from array:");
		for ( NbaTeams team : lowerArray )
			System.out.println(team.toString());
	}
	
	enum NbaTeams {
		CELTICS,	NETS,		KNICKS,			P76ERS,		RAPTORS,
		BULLS,		CAVALIERS,	PISTONS,		PACERS,		BUCKS,
		HAWKS,		HORNETS,	HEAT,			MAGIC,		WIZARDS,
		WARRIORS,	CLIPPERS,	LAKERS,			SUNS,		KINGS,
		MAVERICKS,	ROCKETS,	GRIZZLIES,		PELICANS,
		SPURS,		NUGGETS,	TIMBERWOLVES,	THUNDER,
		BLAZERS,	JAZZ
	}

}
