package com.wellsfromwales.samples;

import java.math.BigInteger;

public class Factorial {

	public static void main(String[] args) {
		System.out.println(factorial(new BigInteger("25")));
	}
	
	static BigInteger factorial( BigInteger num ) {
		if ( num.equals( new BigInteger("0") ) || num.equals( new BigInteger("1") ) )
			return new BigInteger("1");
		return num.multiply( factorial(num.subtract( new BigInteger("1") ) ) );
	}

}
