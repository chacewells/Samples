package com.wellsfromwales.samples;

public class Bird {
	
	public static void main(String[] args) {
		Bird b = new Bird();
		b.sound(5);
	}
	
	public void sound() {
		System.out.println("flap");
	}
	
	public void sound( int num ) {
		for (int i : new int[num]) {
			sound();
		}
	}
}
