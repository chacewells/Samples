package com.wellsfromwales.samples;

import java.util.Stack;

public class RandomList {
	
//	static int SEED = 500;
//
//	public static void main(String[] args) {
//		int[] randomVals = new int[10];
//		for (int i = 0; 
//				i < randomVals.length;
//				System.out.print((new Random(SEED+i++)).nextInt(100) + " "));
//	}
	
	public static void main(String[] args) {
		Stack<String> stack = new Stack<>();
		
		String[] names = {"Aaron","Andrew","Marianne","Ken","AJ","Rehnman","Keri"};
		
		for ( String name : names )
			stack.push(name);
		
		while ( !stack.empty() ) {
			System.out.println(stack.peek());
			System.out.println(stack.pop());
		}
	}

}
