package com.wellsfromwales.samples;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JREP {
	private static BufferedReader reader;

	public static void main(String[] args) throws IOException {
		String regex = args[0];
		reader = getReader(args[1]);
		Pattern pat = Pattern.compile(regex);
		
		while (reader.ready()) {
			String input = reader.readLine();
			printIfMatches(input, pat);
		}
		
		reader.close();
	}
	
	private static BufferedReader getReader(String filename) throws IOException {
		FileReader fReader;
		BufferedReader reader;
		
		fReader = new FileReader(filename);
		reader = new BufferedReader(fReader);
		
		return reader;
	}
	
	private static void printIfMatches( String str, Pattern pat ) throws IOException {
		Matcher matcher = pat.matcher(str);
		if (matcher.find())
			System.out.println(str);
	}

}
