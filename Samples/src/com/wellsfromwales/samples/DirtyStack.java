package com.wellsfromwales.samples;

public class DirtyStack {
	public DSNode head;
	
	public DirtyStack() {
		head = null;
	}
	
	public DirtyStack( int data ) {
		push( data );
	}
	
	public static void main( String...args ) {
		int myInt = 8576832;
		int tmp = myInt;
		int mod;
		DirtyStack myStack = new DirtyStack();
		
		while ( tmp > 0 ) {
			mod = tmp % 10;
			myStack.push( mod );
			tmp /= 10;
		}
		
		while (myStack.head != null)
			System.out.print(myStack.pop());
	}
	
	public void push( int data ) {
		DSNode newNode = new DSNode( data );
		newNode.last = head;
		head = newNode;
	}
	
	public int pop() {
		DSNode popped = head;
		head = popped.last;
		return popped.data;
	}
	
	public class DSNode {
		public DSNode last;
		public int data;
		
		public DSNode( int data ) {
			this.data = data;
			this.last = null;
		}
		
		public DSNode( int data, DSNode last ) {
			this.data = data;
			this.last = last;
		}
	}
}
