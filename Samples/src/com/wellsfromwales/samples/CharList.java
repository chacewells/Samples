package com.wellsfromwales.samples;

import java.util.ArrayList;
import java.util.List;

public class CharList {
	
	private List<Character> charList;
	
	public CharList( Character[] chars ) {
		setList(chars);
	}
	
	public CharList( char[] chars ) {
		setList(chars);
	}
	
	public CharList( String str ) {
		this( str.toCharArray() );
	}
	
	public void setList( char[] chars ) {
		charList = new ArrayList<>(chars.length);
		for ( Character c : chars )
			charList.add(c);
	}
	
	public void setList( String str ) {
		setList( str.toCharArray() );
	}
	
	public void setList( Character[] chars ) {
		charList = new ArrayList<>( chars.length );
		for ( Character c : chars )
			charList.add(c);
	}
	
	public void setList( List<Character> list ) {
		charList = new ArrayList<>( list );
	}
	
	public void add( Character c ) {
		charList.add(c);
	}
	
	public void add( int position, Character c ) {
		charList.add( position, c );
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for ( Character c : charList )
			sb.append(c);
		
		return sb.toString();
	}
	
}
