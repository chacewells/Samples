package com.wellsfromwales.samples;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class WriteReadSample {
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		String filename = "charpairs.dat";
		SimpleCharPair one = new SimpleCharPair('a', 'b');
		SimpleCharPair two = new SimpleCharPair('c', 'd');
		
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
		out.writeObject(one);
		out.writeObject(two);
		out.close();
		
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
		SimpleCharPair pair;
		try {
		while ( (pair = (SimpleCharPair)in.readObject()) != null )
			System.out.println("SimpleCharPair: " + pair.a + " " + pair.b);
		} catch (EOFException e) {
			try {
				if (in != null)
					in.close();
			} catch ( IOException f ){}
		}
	}
	
	public static class SimpleCharPair implements Serializable {
		public char a;
		public char b;
		
		public SimpleCharPair(char a, char b) {
			this.a = a;
			this.b = b;
		}
	}

}
