package com.wellsfromwales.concurrency;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ExecutorServiceDemo {

	public static void main(String[] args) throws IOException,
			InterruptedException, ExecutionException, TimeoutException {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		final PipedOutputStream outputStream = new PipedOutputStream();
		final PipedInputStream inputStream = new PipedInputStream(outputStream);
		
		executor.submit(new Runnable() {
			public void run() {
				try {
					outputStream.write(1);
					outputStream.write(2);
					Thread.sleep(5000);
					outputStream.write(3);
					outputStream.close();
				} catch (InterruptedException | IOException e) {
					e.printStackTrace();
				}
			}
		});

		int readByte = 1;
		Callable<Integer> readTask = new Callable<Integer>() {
			public Integer call() {
				Integer result = -1;
				try {
					result = inputStream.read();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return result;
			}
		};
		
		while (readByte != 0) {
			Future<Integer> future = executor.submit(readTask);
			readByte = future.get(1000, TimeUnit.MILLISECONDS);
			if (readByte >= 0)
				System.out.println("Read: " + readByte);
		}
		
		inputStream.close();

	}

}
