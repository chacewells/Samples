package com.wellsfromwales.concurrency;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PipingDemo {

	public static void main(String[] args) throws IOException,
			InterruptedException, ExecutionException {
		try (final PipedOutputStream pipeOut = new PipedOutputStream();
				final PipedInputStream pipeIn = new PipedInputStream(pipeOut);) {

			PrintWriter pipeWriter = new PrintWriter(new OutputStreamWriter(
					new BufferedOutputStream(pipeOut)));

			BufferedReader keyboard = new BufferedReader(new InputStreamReader(
					System.in));

			ExecutorService executor = Executors.newFixedThreadPool(2);

			executor.submit(new Runnable() {
				public void run() {
					String line;
					try {
						while ((line = keyboard.readLine()) != null) {
							pipeWriter.println(line);
							pipeWriter.flush();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

			Callable<String> pipeOutCallable = new Callable<String>() {
				BufferedReader reader;

				public String call() {
					String result = null;
					if (reader == null) {
						reader = new BufferedReader(new InputStreamReader(
								pipeIn));
					}
					try {
						result = reader.readLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return result;
				}
			};

			String line = null;
			while (true) {
				try {
					Future<String> future = executor.submit(pipeOutCallable);
					line = future.get(5000, TimeUnit.MILLISECONDS);
					System.out.println(line);
				} catch (TimeoutException te) {
					System.err.println("too slow joe!");
				}
			}
		}
	}

}
