package com.wellsfromwales.expression;

public class ExpressionUtilsTest2 {
	
	private static IsHexChar[] testCases = 
		{
		new IsHexChar('f', true),
		new IsHexChar('0', true),
		new IsHexChar('%', false),
		};

	public static void main(String[] args) {
		for (IsHexChar hexTest : testCases )
			hexTest.test();
		
		ExpressionUtilsTest2 eut = new ExpressionUtilsTest2();
		StupidClass o = eut.getStupid();
	}
	
	public StupidClass getStupid() {
		return new StupidClass();
	}
	
	public class StupidClass {
		
	}
	
	private static class IsHexChar {
		private final char hexChar;
		private final boolean expectedResult;
		
		public IsHexChar(char hexChar, boolean expectedResult) {
			super();
			this.hexChar = hexChar;
			this.expectedResult = expectedResult;
		}
		
		public void test() {
			boolean actualResult = ExpressionUtils.isHexChar( hexChar );
			if ( actualResult != expectedResult ) {
				System.out.format("test failed; under test = %u, actual = %b, expectedResult = %b%n",
						hexChar, actualResult, expectedResult);
			}
		}
	}

}
