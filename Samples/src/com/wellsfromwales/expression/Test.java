package com.wellsfromwales.expression;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;

public class Test {

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		BigDecimal bigBangAgo = new BigDecimal(4.35486e20);
		BigDecimal nowMillis = new BigDecimal( cal.getTimeInMillis() );
		BigDecimal bigBangMillis = nowMillis.subtract(bigBangAgo);
		System.out.println(bigBangMillis.toString().replaceAll("-", "").length());
		System.out.println(bigBangMillis);
		System.out.println(bigBangMillis.doubleValue());
		
		Formatter f = new Formatter();
		
		Date date = Calendar.getInstance().getTime();
		
//		X$ notation tells the formatter which position argument to get its format from
		f.format("%TH:%%%1$TM%n", date );
		
//		apparently calling format() on a Formatter object repeatedly
//		will append the new format sequence to the original format string
		f.format("%TH:%<TM", date);
		
//		then you can just call toString() on the formatter object
//		(or pass it into out::println)
		System.out.println(f);
		
		String fmt = String.format("this string formats the number %d as %1$02X", 15);
		System.out.println(fmt);
		System.out.format("this string does the same thing, except lowercase: %d as %<02x", 15);
		
		f.close();
		
		ExpressionUtilsTest2.StupidClass sc = new ExpressionUtilsTest2().new StupidClass();
	}
	
	public static boolean isSymbolChar( char test ) {
		boolean result =
				Character.isAlphabetic( test ) ||
				Character.isDigit( test ) ||
				test == '_';
		
		return result;
	}
	
	public static boolean isHexChar( char test ) {
		boolean result = false;
		
		return result;
	}

}
