package com.wellsfromwales.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Playground {

	public static void main(String[] args) {
		matchOr();
		reluctant();
		greedy();
	}
	
	public static void matchOr() {
		String regex = "cat|batty";
		String cat = "cat";
		String batty = "batty";
		String diorama = "diorama";
		Pattern p = Pattern.compile(regex);
		for (String s : new String[]{cat,batty,diorama}) {
			Matcher m = p.matcher(s);
			System.out.println(m.matches() ? "'" + s + 
					"' matches '" + regex + "'" : "'" + s + "' does not match '"
					+ regex +  "'");
		}
	}
	
	public static void reluctant() {
		String regex = "haza*?";
		String str = "hazaaaaaaaaaaaaaaahazaaa";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		while (m.find())
			System.out.println("reluctant: found '" + m.group() + "' at " + m.start() + ", " + m.end());
	}
	
	public static void greedy() {
		String regex = "haza*";
		String str = "hazaaaaaaaaaaaaaaahazaaa";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		while (m.find())
			System.out.println("greedy: found '" + m.group() + "' at " + m.start() + ", " + m.end());
	}

}
