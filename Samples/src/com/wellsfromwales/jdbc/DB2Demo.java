package com.wellsfromwales.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;


public class DB2Demo {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		new DB2Demo().db2();
	}
	
	public void mysql() throws SQLException {
		Connection cxn = null;
		Properties props = new Properties();
		props.put("user", "root");
		props.put("password", "");
		
		cxn = DriverManager.getConnection("jdbc:mysql://localhost:3306/", props);
		
		Statement statement = cxn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM hello.world");
		while (resultSet.next()) {
			int id = resultSet.getInt("id");
			String sayHello = resultSet.getString("say_hello");
			System.out.println("result: " + id + ": " + sayHello);
		}
	}
	
	public void db2() throws SQLException, ClassNotFoundException {
		Connection cxn = null;
		Properties props = new Properties();
		props.put("user", "student");
		props.put("password", "EXPDWASH");
		
		cxn = DriverManager.getConnection("jdbc:db2://dbadmin2.chq.ei:50000/sqltr", props);
		
		Statement statement = cxn.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM jdyvig.dept");
		ResultSetMetaData meta = resultSet.getMetaData();
		int columns = meta.getColumnCount();
		for (int i = 1; i <= columns; ++i) {
			System.out.print(meta.getColumnName(i) + "\t");
		}
		System.out.println();
		
		while (resultSet.next()) {
			for (int i = 1; i <= columns; ++i) {
				System.out.print(resultSet.getObject(meta.getColumnName(i), Class.forName(meta.getColumnClassName(i))) + "\t");
			}
			System.out.println();
		}
	}

}
