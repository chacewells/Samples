package com.wellsfromwales.encryption;

public class WhenEncryptionIsOutlawed {

	public static void main(String[] args) {
		String str = "When cryptography is outlawed, bayl bhgynjf jvyy unir cevinpl.";
		for (int i = 0;
				i < 26;
				System.out.println(i + ": " + shiftPrint(str, ++i)));
	}
	
	static String shiftPrint(String str, int shift) {
		StringBuilder sb = new StringBuilder();
		for (char c : str.toCharArray()) {
			sb.append( (""+c).matches("\\s") ? c : displace(c, shift) );
		}
		
		return sb.toString();
	}
	
	static char displace(char c, int shift) {
		if ( !Character.isAlphabetic(c) ) { 
			return c;
		}
		
		int offset = Character.isUpperCase(c) ? 65 : 97;
		int result = c % offset;
		return (char) ( ((result + shift) % 26) + offset );
	}

}
