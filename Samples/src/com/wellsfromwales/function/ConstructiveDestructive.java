package com.wellsfromwales.function;

import java.util.function.Function;

public class ConstructiveDestructive {
	public static void main(String[] args) {
//		Function<Integer,Function<Integer,Integer>> f = x -> y -> x + y;
//		int z = f.apply(5).apply(15);
//		System.out.println(z);
		
		System.out.println(
				((Function<
						Integer,Function<Integer,
								Function<Integer,Integer>>>) x -> y -> z -> x + y + z).apply(5).apply(6).apply(7)
		);
	}
}
