package com.wellsfromwales.function;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StreamExample3 {
	public static class PlayerPoints {
		public final String name;
		public final long points;
		public PlayerPoints(String name, long points) {
			super();
			this.name = name;
			this.points = points;
		}
		
		@Override
		public String toString() {
			return name + ":" + points;
		}
	}
	
	public static long getPoints(final String name) {
		if ("John".equalsIgnoreCase(name)) {
			return 10;
		} else if ("Tim".equalsIgnoreCase(name)) {
			return 15;
		} else if ("David".equalsIgnoreCase(name)) {
			return 20;
		}else if ("George".equalsIgnoreCase(name)) {
			return 7;
		}
		return 0;
	}
	
	public static void main(String[] args) {
		String[] names = new String[] {"John", "Tim", "David", "George"};
		PlayerPoints highestPlayer = Arrays.stream(names)
				.map(name -> new PlayerPoints(name, getPoints(name)))
				.reduce(new PlayerPoints("", 0l), (s1, s2) -> (s1.points > s2.points) ? s1: s2);
		
		System.out.println(highestPlayer);
		
		long maxPoint = Arrays.stream(names)
				.map(name -> new PlayerPoints(name, getPoints(name)))
				.filter(p -> p.points > 10)
				.mapToLong( p -> p.points)
				.max().getAsLong();
		System.out.println(maxPoint);
				
	}
}
