package com.wellsfromwales.function;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class LamdaExpression {
	
	public static class Person {
		String firstName;
		String lastName;
		  
		public String getFirstName() {
			return firstName;
		}
		 
		public String getLastName() {
			return lastName;
		}

		@Override
		public String toString() {
			return firstName + " " + lastName;
		}		
	}
	

	public static void main(String[] args) {
		
		sortAndPrintWithLambdaExpression();
		
		sortPersonListWithMethodReference();
	}
	
	public static void sortAndPrintWithLambdaExpression() {
		
		List<String> strList = new ArrayList<String>();
		strList.add("Test");
		strList.add("Java");
		strList.add("Hello");
		
		// Java 7
//		Collections.sort(strList, new Comparator<String>() {
//			@Override
//			public int compare(String o1, String o2) {
//				return o1.compareToIgnoreCase(o2);
//			}
//		});
		
		// Java 8
		strList.sort((s1, s2) -> s1.compareToIgnoreCase(s2));
		strList.forEach(System.out::println);
		
	}	
	
	public static void createActionListenerInJava8() {
		// Java 7
		ActionListener al7 = new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(e.getActionCommand());				
			}
		};
		Button button = new Button();
		button.addActionListener(al7);
		
		// Java 8
		ActionListener al8 = e -> System.out.println(e.getActionCommand());
		button.addActionListener(al8);
	}
	
	public static void sortPersonListWithMethodReference() {
		Person person1 = new Person();
		person1.firstName ="Jane";
		person1.lastName = "Doe";
		
		Person person2 = new Person();
		person2.firstName = "George";
		person2.lastName = "Smith";
		
		Person person3 = new Person();
		person3.firstName = "Lesley";
		person3.lastName = "Miller";
		
		Person person4 = new Person();
		person4.firstName = "Ian";
		person4.lastName = "Miller";
		
		List<Person> persons = new ArrayList<LamdaExpression.Person>();
		persons.add(person1);
		persons.add(person2);
		persons.add(person3);
		persons.add(person4);
		
		// Java 7 sorting
//		Collections.sort(persons, new Comparator<Person>() {
//
//			@Override
//			public int compare(Person o1, Person o2) {
//				int n = o1.getLastName().compareTo(o2.getLastName());
//				if (n == 0) {
//					return o1.getFirstName().compareTo(o2.getFirstName());
//				}
//				return n;
//			}
//		});
		
		// Java 8 sorting
		persons.sort(Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName));
		
		persons.forEach(System.out::println);
	}	
}
