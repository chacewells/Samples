package com.wellsfromwales.function;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


public class FileFilter {
	
	public static void main(String[] args)  throws IOException {
		
//		Files.walk(Paths.get("C:\\Temp"))
//	     .filter(Files::isRegularFile)
//	     .collect(Collectors.toList())
//	     .forEach(path -> System.out.println(path.toString()));
		
		
		System.out.println("Print js files only: ");
		List<File> filesInTemp = getFiles();
		filesInTemp.forEach(file -> {if (fileIsJs(file)) {System.out.println(file.getName());}});
	}
	
	public static boolean fileIsJs(File file) {
		return file.getName().endsWith(".js");
	}
	
	public static boolean fileIsPdf(File file) {
		return file.getName().endsWith(".pdf");
	}
	
	public static boolean fileIsRtf(File file) {
		return file.getName().endsWith(".rtf");
	}
	
	public static List<File> getFiles() throws IOException {
		List<File> filesInFolder = Files.walk(Paths.get("C:\\Temp"))
                .filter(Files::isRegularFile)
                .map(Path::toFile)
                .collect(Collectors.toList());
		return filesInFolder;
	}
	
}
