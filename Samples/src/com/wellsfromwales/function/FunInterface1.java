package com.wellsfromwales.function;

@FunctionalInterface
public interface FunInterface1 {
	int add(String str1, String str2);
	
	//int subtract(String str1, String str2);
	
	default int substract(String str1, String str2) {
		return str1.length() - str2.length();
	}
	
	static int length(String str) {
		return str.length();
	}
}
