package com.wellsfromwales.function;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;



// Streaming file trees and text pattern
public class CsvFileParser {
	public void parseCsv(String filename) throws IOException {
		Files.lines(Paths.get(filename))
		.map(String::trim)
		.forEach(str -> {
			System.out.println("\n\nParse one line: ");
			Pattern patt = Pattern.compile(";");
			patt.splitAsStream(str).forEach(word -> {System.out.print(word + " ");});
			});
	}
	
	public static void main(String[] args) throws IOException {
		CsvFileParser parser = new CsvFileParser();
		parser.parseCsv("C:\\Temp\\WALMDFW021615-030215-1.csv");
	}
}
