package com.wellsfromwales.function;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Curry {
	
	public static void main(String[] args) {
//		this is the on-the-fly lambda implementation
		BiFunction<String,Character,Function<String,String>> makeGreeting = 
				(in,punc) -> name -> in + ", " + name + punc;
		
		Function<String,String> greeting = makeGreeting.apply("hello", '!');
		
		System.out.println(greeting.apply("Aaron"));
		System.out.println(greeting.apply("Jenni"));
		
		final Function<String,String> wazzup = makeGreeting.apply("WAZZAP", '?');
		
		System.out.println(greeting.apply("Fallow"));
		System.out.println(greeting.apply("Crombie"));
		Arrays.asList("Fallow", "Crombie", "The Dude")
			.stream()
			.forEach(n -> System.out.println(wazzup.apply(n)));
	}
	
//	this is the verbose method version
	static Function<String,String> makeGreeting(String in, Character punc) {
		return new Function<String,String>() {
			@Override
			public String apply(String name) {
				return in + ", " + name + punc;
			}
		};
	}
	
}
