package com.wellsfromwales.function;

import java.util.Arrays;
import java.util.function.Supplier;

public class Closure {
	
//	I'm not really sure what I gain by doing this
	static Supplier<Integer> supplier = ((Supplier<Supplier<Integer>>)() -> {
		class IntObj {int value = 0;}
		IntObj obj = new IntObj();
		return () -> ++obj.value;
	}).get();
	
	public static void main(String[] args) {
		for (int i = 0 ; i < 20; ++i) {
			System.out.println(supplier.get());
		}
		
		Arrays.asList(5,10,15,20)
			.stream()
			.forEach(i -> System.out.println(i*i));
	}

}
