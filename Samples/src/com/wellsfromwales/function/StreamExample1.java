package com.wellsfromwales.function;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// Collection and files streaming
public class StreamExample1 {
	public static void main(String[] args) {
		// Streaming Collections
		List<String> strList = new ArrayList<String>();
		strList.add("Test1");
		strList.add("Test2");
		strList.add("Test3");
		
		strList.stream().forEach(str -> System.out.println(FunInterface1.length(str)));
		strList.parallelStream().forEach(System.out::println);
		
		// Streaming Files
		try {			
			System.out.println("\nUse BufferedReader.lines() to get Stream");
			FileReader fr = new FileReader("C:\\Temp\\WALMDFW021615-030215-1.csv");
			BufferedReader br = new BufferedReader(fr);
			br.lines().forEach(System.out::println);
			
			System.out.println("\nUse Files.lines() to get Stream");
			Files.lines(Paths.get("C:\\Temp\\WALMDFW021615-030215-1.csv")).forEach(System.out::println);
			
			System.out.println("\nStreaming File Trees");
			Files.walk(Paths.get("C:\\Temp"))
		     .filter(Files::isRegularFile)
		     .collect(Collectors.toList())
		     .forEach(path -> System.out.println(path.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
