package com.wellsfromwales.function;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.IntStream;


public class StreamExample5 {
	
	public static void main(String[] args) throws Exception{
	ForkJoinPool forkJoinPool = new ForkJoinPool(2);
	forkJoinPool.submit(() -> {
	    //parallel task here, for example
		IntStream.range(1, 10000).parallel().filter(i -> i % 2 == 0).forEach(System.out::println);
		//IntStream.range(1, 1000).parallel().filter(PrimesPrint::isPrime).collect(toList());
	}
	).get();
	}
}
