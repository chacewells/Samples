package com.wellsfromwales.function;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.function.Supplier;

public class DirtyInitialization {
	static Collection<String> initialize = ((Supplier<Collection<String>>)() -> {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(DirtyInitialization.class.getClassLoader().getResourceAsStream("messages.txt")))) {
			Collection<String> collection = new HashSet<>();
			for (
					String line = reader.readLine();
					line != null;
					line = reader.readLine()) {
				collection.add(line);
			}
			return Collections.unmodifiableCollection(collection);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}).get();
	
	public static void main(String[] args) {
	}
	
}
