package com.wellsfromwales.function;

import java.util.function.BiFunction;

public class FunctionExample {
	
	static final Integer A = 15;
	static final Integer B = 10;
	
	public static void main(String[] args) {
		BiFunction<Number, Number,Double> calcHypoteneus = 
				FunctionExample::calcHypoteneus;
		Double c = calcHypoteneus.apply(A, B);
		System.out.println("Got: " + c);
	}
	
	public static Double calcHypoteneus(Number a, Number b) {
		return Math.sqrt(Math.pow(a.doubleValue(), 2) + Math.pow(b.doubleValue(), 2));
	}
	
}
