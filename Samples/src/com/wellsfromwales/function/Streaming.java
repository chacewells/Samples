package com.wellsfromwales.function;
/**
 * This example is provided by Patrick Haas from CRM Group.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

public class Streaming {

	public static class StreamTest implements Callable<Void> {
		private final List<Integer> numbers;
		private final int delay;
		private final String name;

		public StreamTest(String name, List<Integer> numbers, int delay) {
			this.name = name;
			this.numbers = numbers;
			this.delay = delay;
		}

		@Override
		public Void call() throws Exception {
			numbers.parallelStream().forEach(num -> {
				System.out.println(name + "-" + Thread.currentThread() + "-" + num);

				try {
					Thread.sleep(delay * num);
				} catch (Exception e) {
				}
			});
			return null;
		}
	}

	public static void main(String[] args) throws InterruptedException {
//		System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "100");
		//final ExecutorService pool = Executors.newFixedThreadPool(2);
		ForkJoinPool pool = new ForkJoinPool(2);

//		processTwoEquallyMatchedStreams(pool);
		processTwoVeryMismatchedStreams(pool);

		pool.shutdown();
		pool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

	}

	public static void processTwoVeryMismatchedStreams(final ExecutorService pool) throws InterruptedException {
		List<Integer> numbers = new ArrayList<Integer>();
		for (int i = 0; i < 20; i++) {
			numbers.add(i);
		}

		pool.submit(new StreamTest("slow", numbers, 1000));
		// Let the common pool be filled by slow jobs
		Thread.sleep(1000);
		pool.submit(new StreamTest("quick", numbers, 1));
	}

	public static void processTwoEquallyMatchedStreams(final ExecutorService pool) {
		List<Integer> numbers = new ArrayList<Integer>();
		for (int i = 0; i < 50; i++) {
			numbers.add(i);
		}
		pool.submit(new StreamTest("a", numbers, 100));
		pool.submit(new StreamTest("b", numbers, 100));
	}
}
