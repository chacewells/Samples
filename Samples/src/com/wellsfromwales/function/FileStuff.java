package com.wellsfromwales.function;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileStuff {
	
	public static void main(String[] args) throws IOException {
		Files.lines( Paths.get(".\\hello.txt") )
		.map(s -> s.replace(',', ' '))
		.forEach(System.out::println);
	}
	
}
