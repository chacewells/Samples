package com.wellsfromwales.function;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// Infinite stream, range and anything
public class StreamExample2 {
	public static void main(String[] args) {
		//Stream.generate(() -> Math.random()).forEach(System.out::println);
		
		//Stream.iterate(1, i -> i+1).forEach(System.out::println);
		
		IntStream.range(1, 11).forEach(System.out::println);
		
		Stream<Integer> s = Stream.of(1, 2, 3);
		s.filter(i -> i > 2).forEach(System.out::println);
		
		String[] strArray = new String[] {"s1", "s2", "s3"};		
		Stream<Object> s2 = Arrays.stream(strArray);
		s2.forEach(System.out::println);
	}
}
