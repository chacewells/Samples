package com.wellsfromwales.function;
import java.util.ArrayList;
import java.util.List;


public class FindMaxNum {
	public static void main(String[] args) {
		List<Double> list = new ArrayList<Double>();
		list.add(1.5d);
		list.add(3.5d);
		list.add(0.5d);
		list.add(2.5d);
		list.add(11.5d);
		
		// Java 7
		System.out.println(findMaxJava7(list));
		
		// Java 8
		System.out.println(list.stream().reduce(0.0d, Math::max));
		
		// or
		System.out.println(list.stream().mapToDouble(Number::doubleValue).max().getAsDouble());
	}
	
	public static Double findMaxJava7(List<Double> list) {
		double max = 0;
		for (Double d : list) {
			if (d > max) {
				max = d;
			}
		}
		return max;
	}
}
