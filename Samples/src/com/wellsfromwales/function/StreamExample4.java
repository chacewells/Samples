package com.wellsfromwales.function;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// Infinite stream, range and anything
public class StreamExample4 {
	public static void main(String[] args) throws IOException {
		// Peek example
		peek();
		
		// Limit example
		limit();
		
		// Sort example
		sort();
		
		// Sorted() after filter
		Files.list(Paths.get("C:\\eclipse_luna\\workspace\\Java8\\src"))
		.map(Path::getFileName)
		.map(Path::toString)
		.filter(name -> name.endsWith(".java"))
		.sorted()
		.limit(5)
		.forEach(System.out::println);
	}
	
	public static void peek() throws IOException {
		Files.list(Paths.get("."))
		.map(Path::getFileName)
		.peek(System.out::println)
		.forEach(p -> doSomething(p));
	}
	
	public static void doSomething(Path p) {
		System.out.println("In doSomething() ...");
		System.out.println(p.getFileName());
	}
	
	public static void limit() {
		Random rnd = new Random();
		rnd.ints().limit(10).forEach(System.out::println);
	}
	
	public static void sort() {
		Random rnd = new Random();
		// Run time exception
		//rnd.ints().sorted().limit(10).forEach(System.out::println);
		
		// OK
		rnd.ints().limit(10).sorted().forEach(System.out::println);
	}
}
