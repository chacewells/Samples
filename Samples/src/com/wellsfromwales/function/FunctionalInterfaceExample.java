package com.wellsfromwales.function;
import java.util.function.Function;


public class FunctionalInterfaceExample {
	public static void main(String[] args) {
		Function<String, String> atr = (name) -> {return "@" + name;};
		Function<String, Integer> leng = (name) -> name.length();
		Function<String, Integer> leng2 = String::length;
		
		for (String s : args) System.out.println(leng2.apply(s));
		
		FunInterface1 fun1 = (String s1, String s2) -> {return s1.length() + s2.length();};
		System.out.println(fun1.add(args[1], args[2]));
		System.out.println(fun1.substract(args[1], args[2]));
	}
}
