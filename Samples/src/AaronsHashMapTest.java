
public class AaronsHashMapTest {

	public static void main(String[] args) {
		String[][] pairs = {
				{"hello", "world"},
				{"silly", "rabbit"},
				{"ugly", "duckling"},
				{"great", "scott"}
		};
		
		AaronsHashMap map = new AaronsHashMap();
		
		for (String[] pair : pairs) {
			map.put(pair[0], pair[1]);
		}
		
		for (String[] pair : pairs) {
			System.out.println(pair[0] + " " + map.get(pair[0]));
		}
	}

}
