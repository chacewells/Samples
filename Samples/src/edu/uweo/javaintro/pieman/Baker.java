package edu.uweo.javaintro.pieman;

public class Baker {
	private static Object lock = new Object();
	
	public void performTask() {
		synchronized (lock) {
//			do I have enough flour to make a pie?
//			do I have oven space?
//			do I have pie storage space?
//			allocate flour
//			PieMan.allocateFlour()
//			allocate oven
//			PieMan.allocateOven()
		}
//		BAKE THE PIE
	}
}
