package edu.uweo.javaintro.pieman;

public class Deliverer {
	
	private static final Object lock = new Object();
	
	public static void main(String[] args) {
		
	}
	
	public void task() {
		synchronized (lock) {
//			are there pies to deliver?
//			allocate n pies
		}
//		deliver (sleep as long as it takes to deliver a pie)
	}
	
	public void enhancedTask() {
//		if no pies to deliver
//		
			synchronized (lock) {
//				if no pies to deliver
//				wait
			}
	}
}
