package edu.uweo.javaintro.exercises;


public class StringComparing {

	static final String[][] stringPairs = {
			{"Zulu","Albatross"},
			{"Market","market"},
			{"200","30"},
			{"400","40"},
			{"hulu","hulu"}
	};
	
	public static void main(String[] args) {
		for (String[] s : stringPairs)
			compare(s[0], s[1]);
	}
	
	public static void compare(String one, String two) {
		int comp = one.compareTo(two);
		String gl;
		
		if (comp > 0)
			gl = "greater than";
		else if (comp < 0)
			gl = "less than";
		else
			gl = "the same as";
		
		System.out.printf("string '%s' is %s string '%s'%n", one, gl, two);
	}

}
