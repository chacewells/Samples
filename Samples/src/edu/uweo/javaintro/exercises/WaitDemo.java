package edu.uweo.javaintro.exercises;

public class WaitDemo implements Runnable {
	
	private static Object synchObj = new Object();

	public static void main(String[] args) {
		Thread thr = new Thread(new WaitDemo());
		thr.start();
		try {
			Thread.sleep(5000);
			synchronized (synchObj) {
				synchObj.notify();
			}
			thr.join();
			System.out.println("done");
		} catch (InterruptedException e) {}
	}

	@Override
	public void run() {
		try {
			synchronized (synchObj) {
				synchObj.wait();
			}
		} catch ( InterruptedException e ) {}
	}

}
