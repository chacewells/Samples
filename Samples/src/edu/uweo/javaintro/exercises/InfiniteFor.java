package edu.uweo.javaintro.exercises;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InfiniteFor {
	
	static Scanner scan;

	public static void main(String[] args) {
		int num = 0;
		do {
			System.out.print("Enter a number between 1 and 10: ");
			num = getUserGuess();
		} while (num < 1 || num > 10);
		
		if (scan != null)
			scan.close();
	}
	
	public static int getUserGuess() {
		int num = 0;
		try {
			num = (scan = new Scanner(System.in)).nextInt();
		} catch (InputMismatchException e) {
			num = 0;
		}
		
		return num;
	}

}
