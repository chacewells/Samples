package edu.uweo.javaintro.exercises;


public class Temperature {
	private double temp;
	
	public static void main(String...args) {
		double[] temps = {78, 42, 56, 18, 73, 67};
		String[] scales = {"F", "C", "f", "c", "F", "C"};
		for(int i=0;i<temps.length;++i) {
			Temperature t = new Temperature( temps[i], scales[i] );
			System.out.printf("temperature %d: %.2f F; %.2f C%n", i+1, t.getFahrenheit(), t.getCelsius() );
		}
	}
	
	public Temperature( double temp ) {
		this(temp,"C");
	}
	
	public Temperature( double temp, String scale ) {
		this.temp = convertIfFahrenheit( temp, scale );
	}
	
	private static double convert( double temp, String scale ) {
		double result = 0.;
		String upScale = scale.toUpperCase();
		if ( upScale.startsWith("F") ) {
			result = (temp - 32) * 5 / 9;
		}
		if ( upScale.startsWith("C") ) {
			result = temp * 9 / 5 + 32;
		}
		
		return result;
	}
	
	private static double convertIfFahrenheit( double temp, String scale ) {
		return 
				( scale.toUpperCase().startsWith("F") ) 
				? convert( temp, scale ) 
				: temp;
	}
	
	public double getCelsius() {
		return temp;
	}
	
	public double getFahrenheit() {
		return convert( temp, "C" );
	}
	
	public void setCelsius( double temp ) {
		this.temp = temp;
	}
	
	public void setFahrenheit( double temp ) {
		this.temp = convert( temp, "F" );
	}
}
