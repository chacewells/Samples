package edu.uweo.javaintro.exercises;

import java.awt.Color;

import edu.uweo.javaintro.Turtle;

public class TwistedTurtle extends Turtle {
	int switchy = 0;
	Color[] colors = {
			Color.CYAN,
			Color.MAGENTA,
			Color.GREEN,
			Color.RED
	};
	
	public static void main( String...args ) throws InterruptedException {
		TwistedTurtle t = new TwistedTurtle(0);
		t.fillCircle( 256 );
		t.drawSomeCrazyShit();
	}
	
	public TwistedTurtle( double newDirection ) {
		this.move( newDirection, 0 );
	}
	
	public Turtle drawSomeCrazyShit() throws InterruptedException {
		int current = 0;
		for (int i = 0; i < 360; i++ ) {
			for ( int j = 0; j < 2; j++ ) {
				paint( 0, i );
				move( 180, i );
				move( 180, 0 );
			}
			if ( i % 90 == 0 )
				switchTo( colors[ current++ ] );
			move( 1, 0 );
		}
		return this;
	}
	
}
