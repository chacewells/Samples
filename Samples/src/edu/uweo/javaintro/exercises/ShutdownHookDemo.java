package edu.uweo.javaintro.exercises;


//	shows how to add a shutdown hook to runtime environment
public class ShutdownHookDemo {
	
	static Object lock = new Object();
	
	public static void main(String[] args) throws InterruptedException {
		Thread shutdownHook = new Thread( new ShutdownHookRunnable() );
		Runtime r = Runtime.getRuntime();
		r.addShutdownHook(shutdownHook);
		
		Thread.sleep(5000);
		System.exit(0);
	}
	
	private static class ShutdownHookRunnable implements Runnable {

		@Override
		public void run() {
			System.out.println("running cleanup code here");
		}
		
	}

}
