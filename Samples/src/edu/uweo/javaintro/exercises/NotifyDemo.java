package edu.uweo.javaintro.exercises;

import java.util.ArrayList;
import java.util.List;

public class NotifyDemo {
	
	static Object lock = new Object();
	
	public static void main(String[] args) {
		List<Thread> threads = new ArrayList<>();
		threads.add( new Thread( new WakeMeUp(), "thread 1" ) );
		threads.add( new Thread( new WakeMeUp(), "thread 2" ) );
		threads.add( new Thread( new WakeMeUp(), "thread 3" ) );
		threads.add( new Thread( new WakeMeUp(), "thread 4" ) );
		
		for ( Thread t : threads )
			t.start();
		
		try {
			Thread.sleep(3);
			synchronized (lock) {
				lock.notifyAll();
			}
			for ( Thread t : threads )
				t.join();
		} catch ( InterruptedException e ) {}
	}
	
	private static class WakeMeUp implements Runnable {

		@Override
		public void run() {
			synchronized (lock) {
				try {
					lock.wait();
					Thread currentThread = Thread.currentThread();
					System.out.println("waking " + currentThread.getName() );
//					Thread.sleep(3000);
				} catch (InterruptedException e ) {}
				
			}
		}
		
	}

}
