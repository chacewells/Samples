package edu.uweo.javaintro.exercises;

import javax.swing.JOptionPane;

public class JOptionPaneDemo3 {

	public static void main(String[] args) {
		
		int option = JOptionPane.showConfirmDialog( null, "Are you sure?" );
		
		switch (option) {
		case JOptionPane.YES_OPTION: System.out.println( "Okay, let's do it!" ); break;
		case JOptionPane.NO_OPTION: System.out.println( "Let's wait up on that." ); break;
		case JOptionPane.CANCEL_OPTION: System.out.println( "Well, nevermind then!" ); break;
		default: System.out.println( "Where'd you go?" );
		}
		
	}

}
