package edu.uweo.javaintro.exercises;

import javax.swing.JOptionPane;

public class JOptionPaneDemo1 {

	public static void main(String[] args) {
		
		String msg = "Hello World";
		JOptionPane.showMessageDialog( null, msg );
		System.out.println( "Done" );
		
	}

}
