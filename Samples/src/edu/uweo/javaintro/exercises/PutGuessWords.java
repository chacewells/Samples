package edu.uweo.javaintro.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class PutGuessWords {
	
	public static final String FILENAME =
			"C:\\Users\\chq-aaronwe\\Documents\\tmp\\secretwords.txt";

	public static void main(String[] args) throws FileNotFoundException {
		String[] guessWords = {
				"duck",
				"cat",
				"dog",
				"cow",
				"squirrel",
				"chipmonk",
				"dummy",
				"play",
				"never",
				"castle",
				"guns"
		};
		
		PrintWriter writer = new PrintWriter(new File(FILENAME));
		
		for (String word : guessWords)
			writer.println(word);
		
		writer.close();
	}

}
