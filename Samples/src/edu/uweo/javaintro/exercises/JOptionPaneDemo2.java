package edu.uweo.javaintro.exercises;

import javax.swing.JOptionPane;

public class JOptionPaneDemo2 {

	public static void main(String[] args) {
		
		String data = JOptionPane.showInputDialog( null, "enter data" );
		if ( data == null || data.length() == 0 )
			JOptionPane.showMessageDialog( null, "you didn't enter anything!" );
		else
			JOptionPane.showMessageDialog( null, "your input string: " + data);
		
	}

}
