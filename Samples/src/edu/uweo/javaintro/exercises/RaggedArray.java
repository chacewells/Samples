package edu.uweo.javaintro.exercises;

public class RaggedArray {

	public static void main(String[] args) {
		double[][] ragged = new double[3][];
		ragged[0] = new double[]{1.2, 3, 2.1};
		ragged[1] = new double[]{15., 20.5, 12.7, 72, 154.9};
		ragged[2] = new double[]{.4, 7923.2};
		
		for ( int inx = 0; inx < ragged.length; System.out.println(), inx++ )
			for (int jnx = 0; jnx < ragged[inx].length; jnx++ )
				System.out.print( ragged[inx][jnx] + " " );
	}

}
