package edu.uweo.javaintro.exercises;

public class Geom {

	public static void main(String[] args) {
		System.err.println(area(1.));
		System.out.println(area(1.));
	}
	
	public static double area( double radius ) {
		double area = Math.PI * radius * radius;
		
		return area;
	}

}
