package edu.uweo.javaintro.exercises;

import javax.swing.JOptionPane;

public class BasicGame {
	String secretWord_ = "rabbit";
	String usersGuess_;
	int numGuesses_;

	public static void main(String[] args) {
		BasicGame game = new BasicGame();
		game.playManyGames();
	}
	
	public void playManyGames() {
		int again = 0;
		
		do {
			playOneGame();
			again = JOptionPane.showConfirmDialog( null, "Again?" );
		} while ( again == JOptionPane.YES_OPTION );
		
	}
	
	public void playOneGame() {
		init();
		askUsersFirstGuess();
		++numGuesses_;
		
		while ( shouldContinue() ) {
			++numGuesses_;
			showResultDialog();
			askUsersNextGuess();
		}
		
		showFinalDialog();
		
	}
	
	protected void askUsersFirstGuess() {
		usersGuess_ = JOptionPane.showInputDialog( "Guess the secret word." );
	}
	
	protected void showResultDialog() {
		JOptionPane.showMessageDialog( null, "That's not it.");
	}
	
	protected void askUsersNextGuess() {
		usersGuess_ = JOptionPane.showInputDialog("Guess again. Hint: it hops");
	}
	
	protected void showFinalDialog() {
		if ( usersGuess_.equals(secretWord_) )
			JOptionPane.showMessageDialog( null, "Good job! That's it!" );
		else
			JOptionPane.showMessageDialog( null, "Sorry! Better luck next time!");
	}
	
	protected boolean shouldContinue() {
		return !usersGuess_.equals(secretWord_)
				&& numGuesses_ < 3;
	}
	
	protected void init() {
		usersGuess_ = "none";
		numGuesses_ = 0;
	}

}
