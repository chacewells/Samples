package edu.uweo.java2.rmi.helloworld.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import edu.uweo.java2.rmi.helloworld.server.HWorld;

public class HWorldClient {

	public static void main(String[] args) {
		new HWorldClient().execute();
	}
	
	public void execute() {
		try {
			Registry 	registry = LocateRegistry.getRegistry();
			HWorld		remoteObject = (HWorld) registry.lookup(HWorld.RMI_NAME);
			String msg = remoteObject.getComment();
			System.out.println(msg);
		} catch (RemoteException | NotBoundException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}

}
