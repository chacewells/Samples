package edu.uweo.java2.rmi.helloworld.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class HWorldServer {

	public static void main(String[] args) {
		new HWorldServer().execute();
	}
	
	public void execute() {
		try {
			HWorldImpl 	impl = 		new HWorldImpl();
			System.out.println("got HWorldImpl");
			HWorld 		stub = 		(HWorld) UnicastRemoteObject
									.exportObject(impl, 0);
			System.out.println("Exported object");
			Registry	registry = 	LocateRegistry.getRegistry();
			System.out.println("got registry");
			registry.rebind( HWorld.RMI_NAME, stub );
			System.out.println("bind successful!");
		} catch (RemoteException e) {
			System.out.println(e.getMessage());
		}
	}

}
