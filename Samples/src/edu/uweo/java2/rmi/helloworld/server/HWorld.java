package edu.uweo.java2.rmi.helloworld.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface HWorld extends Remote {
	public static final String RMI_NAME = "rmi:hworld";
	String getComment() throws RemoteException;
}
