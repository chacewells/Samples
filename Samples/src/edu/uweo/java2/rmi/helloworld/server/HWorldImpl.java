package edu.uweo.java2.rmi.helloworld.server;

import java.rmi.RemoteException;

public class HWorldImpl implements HWorld {

	@Override
	public String getComment() throws RemoteException {
		return "hello world!";
	}

}
