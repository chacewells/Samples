import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class SerializeDeserialize {
	
	static class SimpleData implements Serializable {
		final String str;
		final int num;
		public SimpleData(String strIn, int numIn) {
			str = strIn;
			num = numIn;
		}
		public String toString() {
			return str + ": " + num;
		}
	}

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		String filename = "ser.dat";
		
		try (ObjectOutputStream out = new ObjectOutputStream(
				new BufferedOutputStream(
						new FileOutputStream(filename)))) {
			SimpleData[] objs = {
					new SimpleData("hello", 155),
					new SimpleData("goodbye", -206)
			};
			for (SimpleData o : objs) {
				out.writeObject(o);
			}
		}
		
		try (ObjectInputStream in = new ObjectInputStream(
				new FileInputStream(filename))) {
			Object o;
			
			while ((o = in.readObject()) != null) {
				System.out.println(o);
			}
		} catch (EOFException e) {
//			moving on...
		}
	}

}
